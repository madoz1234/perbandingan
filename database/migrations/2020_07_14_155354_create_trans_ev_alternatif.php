<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransEvAlternatif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_ev_alternatif', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alternatifid1')->unsigned()->default(1);
            $table->integer('alternatifid2')->unsigned()->default(1);
            $table->integer('kriteria_id')->unsigned()->default(1);
            $table->float('nilai', 5, 3);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('alternatifid1')->references('id')->on('ref_alternatif');
            $table->foreign('alternatifid2')->references('id')->on('ref_alternatif');
            $table->foreign('kriteria_id')->references('id')->on('ref_kriteria');
        });

        Schema::create('log_trans_ev_alternatif', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('alternatifid1')->unsigned()->default(1);
            $table->integer('alternatifid2')->unsigned()->default(1);
            $table->integer('kriteria_id')->unsigned()->default(1);
            $table->float('nilai', 5, 3);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_ev_alternatif');
        Schema::dropIfExists('trans_ev_alternatif');
    }
}
