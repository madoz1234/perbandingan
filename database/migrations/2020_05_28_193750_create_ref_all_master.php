<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefAllMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_kriteria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 200)->nullable();
            $table->string('nama', 200);
            $table->integer('bobot');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_alternatif', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 200);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_kriteria', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('kode', 200)->nullable();
            $table->string('nama', 200);
            $table->integer('bobot');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_alternatif', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('nama', 200);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('log_ref_alternatif');
    	Schema::dropIfExists('log_ref_kriteria');
    	Schema::dropIfExists('ref_alternatif');
        Schema::dropIfExists('ref_kriteria');
    }
}
