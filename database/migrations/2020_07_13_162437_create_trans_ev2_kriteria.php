<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransEv2Kriteria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_ev2_kriteria', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kriteriaid1')->unsigned()->default(1);
            $table->integer('kriteriaid2')->unsigned()->default(1);
            $table->integer('ev')->unsigned()->default(1);
            $table->float('nilai', 5, 3);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('kriteriaid1')->references('id')->on('ref_kriteria');
            $table->foreign('kriteriaid2')->references('id')->on('ref_kriteria');
        });

        Schema::create('log_trans_ev2_kriteria', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('kriteriaid1')->unsigned()->default(1);
            $table->integer('kriteriaid2')->unsigned()->default(1);
            $table->integer('ev')->unsigned()->default(1);
            $table->float('nilai', 5, 3);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_ev2_kriteria');
        Schema::dropIfExists('trans_ev2_kriteria');
    }
}
