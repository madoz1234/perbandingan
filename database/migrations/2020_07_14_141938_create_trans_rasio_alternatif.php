<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransRasioAlternatif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_rasio_alternatif_ahp', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alternatif_1_id')->unsigned()->default(1);
            $table->integer('alternatif_2_id')->unsigned()->default(1);
            $table->integer('kriteria_id')->unsigned()->default(1);
            $table->integer('ratio');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('kriteria_id')->references('id')->on('ref_kriteria');
            $table->foreign('alternatif_1_id')->references('id')->on('ref_alternatif');
            $table->foreign('alternatif_2_id')->references('id')->on('ref_alternatif');
        });

        Schema::create('log_trans_rasio_alternatif_ahp', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('alternatif_1_id')->unsigned()->default(1);
            $table->integer('alternatif_2_id')->unsigned()->default(1);
            $table->integer('kriteria_id')->unsigned()->default(1);
            $table->integer('ratio');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_rasio_alternatif_ahp');
        Schema::dropIfExists('trans_rasio_alternatif_ahp');
    }
}
