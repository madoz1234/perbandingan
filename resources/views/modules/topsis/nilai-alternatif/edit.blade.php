<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Ubah Data Nilai Alternatif</h5>
    </div>
    <div class="modal-body">
        <div class="form-group field">
            <label class="control-label">Nama Alternatif</label>
            <select class="selectpicker form-control show-tick" data-size="3" name="alternatif_id" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                @foreach(App\Models\Master\Alternatif::where(function ($a) use ($record) {
                            $a->doesntHave('nilaialternatif')->orWhere('id', $record->alternatif_id);
                        })->get() as $alternatif)
                    <option value="{{ $alternatif->id }}" @if($alternatif->id == $record->alternatif_id) selected @endif>{{ $alternatif->nama }}</option>
                @endforeach
            </select>
        </div>
        @foreach($kriteria as $kriterias)
            <div class="form-group field">
                <label class="control-label">{{ $kriterias->nama }}</label>
                @if($record->detail->where('nilai_id', $record->id)->where('kriteria_id', $kriterias->id)->first())
                    <input type="text" name="detail[{{ $kriterias->id }}][nilai]" class="form-control" placeholder="Nilai {{ $kriterias->nama }}" required="" value="{{ $record->detail->where('nilai_id', $record->id)->where('kriteria_id', $kriterias->id)->first()->nilai }}">
                @else 
                    <input type="text" name="detail[{{ $kriterias->id }}][nilai]" class="form-control" placeholder="Nilai {{ $kriterias->nama }}" required="" value="">
                @endif
            </div>
        @endforeach
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>