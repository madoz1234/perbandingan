<form action="" id="formData">
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Detil Nilai Alternatif</h5>
    </div>
    <div class="modal-body">
    	<table class="table table-bordered" style="font-size: 12px;">
			<tbody>
				<tr>
					<td style="width: 200px;">Nama Alternatif</td>
					<td style="width: 10px;">:</td>
					<td style="text-align: left;">
						{{ $record->alternatif->nama }}
					</td>
				</tr>
				@foreach($record->detail as $detail)
					<tr>
						<td style="width: 200px;">{{ $detail->kriteria->nama }}</td>
						<td style="width: 10px;">:</td>
						<td style="text-align: left;">
							{{ $detail->nilai }}
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
    </div>
    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>
