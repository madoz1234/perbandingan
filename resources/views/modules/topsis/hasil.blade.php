@extends('layouts.base')

@push('css')
<link rel="stylesheet" href="#" type="text/css" />
<link rel="stylesheet" href="{{ asset('libs/assets/one-calender/simple-calendar.css') }}" type="text/css" />
@endpush

@push('js')
<script src="{{ asset('libs/assets/one-calender/jquery.simple-calendar.js') }}"></script>
{{-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="https://apexcharts.com/samples/assets/stock-prices.js') }}"></script> --}}
@endpush

@section('title', 'Hasil TOPSIS')

@section('side-header')
<div style="margin-right: 14px;">
  <span><i class="glyphicon glyphicon-home"></i></span>
</div>
@endsection

@push('styles')
@endpush

@push('scripts')
@endpush

@section('body')
<div class="row row-sm">
  <div class="panel b-a">
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Matriks Keputusan</span>          
    </div>
    <div class="wrapper b-b b-light">
      <table id="example" class="table table-bordered m-t-none" style="width: 100%;">
        <tbody>
          <tr>
            <td style="text-align: center;font-weight: bold;" rowspan="2">Alternatif</td>
            <td style="text-align: center;font-weight: bold;" colspan={{count($kriteria)}}>Kriteria</td>
          </tr>
          <tr>
            @foreach($kriteria as $kriterias)
            <td style="">{{$kriterias->nama}}</td>
            @endforeach
          </tr>
          @foreach($alternatif as $data)
          <tr>
            <td style="">{{ $data->nama }}</td>
            @php
            $n = $nilai->where('alternatif_id', $data->id)->first();
            @endphp
            @foreach($n->detail as $details)
            <td style="">{{$details->nilai}}</td>
            @endforeach
          </tr>
          @endforeach
        </tbody>
      </table>
      <table id="example" class="table table-bordered m-t-none" style="width: 100%;">
        <tbody>
          <tr>
            <td style="text-align: center;font-weight: bold;" rowspan="2">Bobot W</td>
            <td style="text-align: center;font-weight: bold;" colspan={{count($kriteria)}}>Kriteria</td>
          </tr>
          <tr>
            @foreach($kriteria as $kriterias)
            <td style="">{{$kriterias->nama}}</td>
            @endforeach
          </tr>
          <tr>
            <td style="">Nilai</td>
            @foreach($kriteria as $kriterias)
            <td style="">{{$kriterias->bobot}}</td>
            @endforeach
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="panel b-a">
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Matriks Normalisasi</span>          
    </div>
    <div class="wrapper b-b b-light">
      <table id="example" class="table table-bordered m-t-none" style="width: 100%;">
        <tbody>
          <tr>
            <td style="text-align: center;font-weight: bold;" rowspan="2">Alternatif</td>
            <td style="text-align: center;font-weight: bold;" colspan={{count($kriteria)}}>Kriteria</td>
          </tr>
          <tr>
            @foreach($kriteria as $kriterias)
            <td style="">{{$kriterias->nama}}</td>
            @endforeach
          </tr>
          @foreach($alternatif as $data)
          <tr>
            <td style="">{{ $data->nama }}</td>
            @php
            $n = $nilai->where('alternatif_id', $data->id)->first();
            @endphp
            @foreach($n->detail as $details)
            @php 
            $cek =0;
            $matrik = $nilaidetail->where('kriteria_id', $details->kriteria_id);
            foreach($matrik as $m){
              $cek += pow($m->nilai, 2);
            }
            @endphp
            <td style="">{{ round(($details->nilai / sqrt($cek)), 5) }}</td>
            @php 
              $sav = App\Models\Topsis\NilaiAlternatifDetail::find($details->id);
              $angka = round(($details->nilai / sqrt($cek)), 5);
              $bobot = $sav->kriteria->bobot;
              $sav->normalisasi_terbobot = ($bobot*$angka);
              $sav->save();
              $maxs = App\Models\Topsis\NilaiAlternatifDetail::where('kriteria_id', $details->kriteria_id)->max('normalisasi_terbobot');
              $mins = App\Models\Topsis\NilaiAlternatifDetail::where('kriteria_id', $details->kriteria_id)->min('normalisasi_terbobot');
              $simpan = App\Models\Topsis\NilaiAlternatifDetail::find($details->id);
              $positif = round(sqrt($maxs - ($bobot*$angka)), 5);
              $negatif = round(sqrt($mins - ($bobot*$angka)), 5);
              if(is_nan($positif)){
                $pos = 0;
              }else{
                $pos = $positif;
              }

              if(is_nan($negatif)){
                $neg = 0;
              }else{
                $neg = $negatif;
              }
              $simpan->positif = $pos;
              $simpan->negatif = $neg;
              $simpan->save();
            @endphp
            @endforeach
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div class="panel b-a">
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Matriks Solusi Ideal Positif</span>          
    </div>
    <div class="wrapper b-b b-light">
      <table id="example" class="table table-bordered m-t-none" style="width: 100%;">
        <tbody>
          <tr>
            <td style="text-align: center;font-weight: bold;">Alternatif</td>
            <td style="text-align: center;font-weight: bold;">Nilai</td>
          </tr>
          @foreach($alternatif as $a => $data)
          <tr>
            <td style="">{{$data->nama}}</td>
            <td style="">
              @php 
              $sav = App\Models\Topsis\NilaiAlternatif::where('alternatif_id', $data->id)->first();
              $detail = round(sqrt($sav->detail->sum('positif')), 5);
              @endphp
              {{$detail}}
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  {{-- <div class="panel b-a">
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Matriks Solusi Ideal Negatif</span>          
    </div>
    <div class="wrapper b-b b-light">
      <table id="example" class="table table-bordered m-t-none" style="width: 100%;">
        <tbody>
          <tr>
            <td style="text-align: center;font-weight: bold;">Alternatif</td>
            <td style="text-align: center;font-weight: bold;">Nilai</td>
          </tr>
          @foreach($alternatif as $a => $data)
          <tr>
            <td style="">{{$data->nama}}</td>
            <td style="">
              @php 
              $sav = App\Models\Topsis\NilaiAlternatif::where('alternatif_id', $data->id)->first();
              $detail = round(sqrt($sav->detail->sum('negatif')), 5);
              @endphp
              {{$detail}}
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div> --}}
  <div class="panel b-a">
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Hasil Preferensi</span>          
    </div>
    <div class="wrapper b-b b-light">
      <table id="example" class="table table-bordered m-t-none" style="width: 100%;">
        <tbody>
          <tr>
            <td style="text-align: center;font-weight: bold;">Alternatif</td>
            <td style="text-align: center;font-weight: bold;">Nilai</td>
          </tr>
          @foreach($alternatif as $a => $data)
          <tr>
            <td style="">{{$data->nama}}</td>
            <td style="">
              @php 
              $sav = App\Models\Topsis\NilaiAlternatif::where('alternatif_id', $data->id)->first();
              $positif = round(sqrt($sav->detail->sum('positif')), 5);
              $negatif = round(sqrt($sav->detail->sum('negatif')), 5);
              if($negatif > 0){
                $hasil = round(($negatif / ($negatif + $positif)), 5);
              }else{
                $hasil = round(($negatif + $positif), 5);
              }

              $simpan = App\Models\Topsis\NilaiAlternatif::find($sav->id);
              $simpan->nilai = $hasil;
              $simpan->save();
              @endphp
              {{$hasil}}
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div class="panel b-a">
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Perangkingan</span>          
    </div>
    <div class="wrapper b-b b-light">
      <table id="example" class="table table-bordered m-t-none" style="width: 100%;">
        <tbody>
          <tr>
            <td style="text-align: center;font-weight: bold;">Alternatif</td>
            <td style="text-align: center;font-weight: bold;">Nilai</td>
          </tr>
          @foreach($nilai->sortByDesc('nilai') as $a => $data)
          <tr>
            <td style="">{{$data->alternatif->nama}}</td>
            <td style="">
              {{$data->nilai}}
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection