@extends('layouts.base')

@push('css')
<link rel="stylesheet" href="#" type="text/css" />
<link rel="stylesheet" href="{{ asset('libs/assets/one-calender/simple-calendar.css') }}" type="text/css" />
@endpush

@push('js')
<script src="{{ asset('libs/assets/one-calender/jquery.simple-calendar.js') }}"></script>
<script src="http://malsup.github.com/jquery.form.js"></script> 
{{-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="https://apexcharts.com/samples/assets/stock-prices.js') }}"></script> --}}
@endpush

@section('title', 'Hasil AHP')

@section('side-header')
<div style="margin-right: 14px;">
  <span><i class="glyphicon glyphicon-home"></i></span>
</div>
@endsection

@push('styles')
@endpush

@push('scripts')
@endpush

@section('body')
<div class="row row-sm">
  <div class="panel b-a">
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Input Perbandingan Berpasangan</span>          
    </div>
    <div class="panel-body" style="padding-bottom: 0px;">
      <form action="{{ route($routes.'.simpan', 1) }}" method="POST" id="formData">
        @method('PATCH')
        @csrf
        <div class="loading dimmer padder-v" style="display: none;">
            <div class="loader"></div>
        </div>
        <table id="example" class="table table-bordered m-t-none" style="width: 100%;">
          <tbody>
            <tr>
              <td>Kriteria</td>
              @foreach($kriteria as $kriterias)
                <td>{{ $kriterias->nama }}</td>
              @endforeach
            </tr>
            @foreach($kriteria as $key => $kriteriaz)
            <tr>
              <td>{{ $kriteriaz->nama }}</td>
              @foreach($kriteria as $keys => $kriterias)
                <td>
                  @if($kriterias->id == $kriteriaz->id)
                    @php 
                      $cek = $ahpkriteria->where('kriteria_1_id',$kriteriaz->id)->where('kriteria_2_id',$kriterias->id)->first();
                    @endphp
                    @if($cek)
                      <input type="hidden" name="detail[{{$key}}{{$keys}}][id]" value="{{ $cek->id }}">
                    @endif
                    <input type="hidden" name="detail[{{$key}}{{$keys}}][kriteria1_id]" value="{{ $kriteriaz->id }}">
                    <input type="hidden" name="detail[{{$key}}{{$keys}}][kriteria2_id]" value="{{ $kriterias->id }}">
                    <input type="hidden" name="detail[{{$key}}{{$keys}}][ratio]" value="1">
                   1/1
                  @else
                    @php 
                      $cekz = $ahpkriteria->where('kriteria_1_id',$kriteriaz->id)->where('kriteria_2_id',$kriterias->id)->first();
                    @endphp
                    @if($cekz)
                      <input type="hidden" name="detail[{{$key}}{{$keys}}][id]" value="{{ $cekz->id }}">
                    @endif
                    <input type="hidden" name="detail[{{$key}}{{$keys}}][kriteria1_id]" value="{{ $kriteriaz->id }}">
                    <input type="hidden" name="detail[{{$key}}{{$keys}}][kriteria2_id]" value="{{ $kriterias->id }}">
                    @if($cekz)
                      <select class="selectpicker form-control show-tick" data-size="3" name="detail[{{$key}}{{$keys}}][ratio]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                        <option value="1" @if($cekz->ratio == 1) selected @endif>Sama penting dengan</option>
                        <option value="2" @if($cekz->ratio == 2) selected @endif>Sedikit lebih penting dari</option>
                        <option value="3" @if($cekz->ratio == 3) selected @endif>Lebih penting dari</option>
                        <option value="4" @if($cekz->ratio == 4) selected @endif>Sangat penting dari</option>
                        <option value="5" @if($cekz->ratio == 5) selected @endif>Mutlak sangat penting dari</option>
                      </select>
                    @else 
                      <select class="selectpicker form-control show-tick" data-size="3" name="detail[{{$key}}{{$keys}}][ratio]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                        <option value="1">Sama penting dengan</option>
                        <option value="2">Sedikit lebih penting dari</option>
                        <option value="3">Lebih penting dari</option>
                        <option value="4">Sangat penting dari</option>
                        <option value="5">Mutlak sangat penting dari</option>
                      </select>
                    @endif
                  @endif
                </td>
              @endforeach
            </tr>
            @endforeach
          </tbody>
        </table>
        <p>&nbsp;</p>
        <div class="form-row">
          <div class="form-group col-md-12">
            <div class="text-right">
              <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
              <button type="button" class="btn btn-simpan save as page">Simpan</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="panel b-a">
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Perbandingan Berpasangan</span>          
    </div>
    <div class="panel-body" style="padding-bottom: 0px;">
        <table id="examples" class="table table-bordered m-t-none" style="width: 100%;">
          <tbody>
            <tr>
              <td>Kriteria</td>
              @foreach($kriteria as $kriterias)
                <td>{{ $kriterias->nama }}</td>
              @endforeach
            </tr>
            @foreach($kriteria as $key => $kriteriaz)
            <tr>
              <td>{{ $kriteriaz->nama }}</td>
              @foreach($kriteria as $keys => $kriterias)
                <td>
                  @if($kriterias->id == $kriteriaz->id)
                    1/1
                  @else
                      @php 
                        $cekz = $ahpkriteria->where('kriteria_1_id',$kriteriaz->id)->where('kriteria_2_id',$kriterias->id)->first();
                        $ceks = $ahpkriteria->where('kriteria_1_id',$kriterias->id)->where('kriteria_2_id',$kriteriaz->id)->first();
                      @endphp
                      @if($cekz && $ceks)
                      {{$cekz->ratio}} / {{$ceks->ratio}}
                      @else 
                        0
                      @endif
                  @endif
                </td>
              @endforeach
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>
  </div>
  <div class="panel b-a">
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Normalisasi 1</span>          
    </div>
    <div class="panel-body" style="padding-bottom: 0px;">
        <table id="examples" class="table table-bordered m-t-none" style="width: 100%;">
          <tbody>
            <tr>
              <td>Kriteria</td>
              @foreach($kriteria as $kriterias)
                <td>{{ $kriterias->nama }}</td>
              @endforeach
            </tr>
            @foreach($kriteria as $key => $kriteriaz)
            <tr>
              <td>{{ $kriteriaz->nama }}</td>
              @foreach($kriteria as $keys => $kriterias)
                <td>
                  @if($kriterias->id == $kriteriaz->id)
                    (1/1) = 1
                      @php 
                        $hasil=1;
                      @endphp
                  @else
                      @php 
                        $cekz = $ahpkriteria->where('kriteria_1_id',$kriteriaz->id)->where('kriteria_2_id',$kriterias->id)->first();
                        $ceks = $ahpkriteria->where('kriteria_1_id',$kriterias->id)->where('kriteria_2_id',$kriteriaz->id)->first();
                        if($cekz && $ceks){
                          $hasil = ($cekz->ratio / $ceks->ratio);
                        }else{
                          $hasil =0;
                        }
                      @endphp
                      @if($cekz && $ceks)
                      ({{$cekz->ratio}} / {{$ceks->ratio}}) = {{round($hasil, 2)}}
                      @else
                        (0 / 0) = 0
                      @endif
                  @endif
                  @php 
                    $cari = App\Models\Ahp\EVAHPKriteria::where('kriteriaid1', $kriterias->id)->where('kriteriaid2', $kriteriaz->id)->first();
                    if($cari){
                      $sav = App\Models\Ahp\EVAHPKriteria::find($cari->id);
                      $sav->kriteriaid1 = $kriterias->id;
                      $sav->kriteriaid2 = $kriteriaz->id;
                      $sav->nilai = round($hasil, 2);
                      $sav->save();
                    }else{
                      $sav = new App\Models\Ahp\EVAHPKriteria;
                      $sav->kriteriaid1 = $kriterias->id;
                      $sav->kriteriaid2 = $kriteriaz->id;
                      $sav->nilai = round($hasil, 2);
                      $sav->save();
                    }
                  @endphp
                </td>
              @endforeach
            </tr>
            @endforeach
            <tr>
              <td>Hasil Penjumlahan</td>
              @foreach($kriteria as $kriterias)
                @php 
                  $cari1 = App\Models\Ahp\EVAHPKriteria::where('kriteriaid1', $kriterias->id)->get();
                @endphp
                  <td>{{ $cari1->sum('nilai') }}</td>
              @endforeach
            </tr>
          </tbody>
        </table>
    </div>
  </div>

  <div class="panel b-a">
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Normalisasi 2</span>          
    </div>
    <div class="panel-body" style="padding-bottom: 0px;">
        <table id="examples" class="table table-bordered m-t-none" style="width: 100%;">
          <tbody>
            <tr>
              <td>Kriteria</td>
              @foreach($kriteria as $kriterias)
                <td>{{ $kriterias->nama }}</td>
              @endforeach
            </tr>
            @foreach($kriteria as $key => $kriteriaz)
            <tr>
              <td>{{ $kriteriaz->nama }}</td>
              @foreach($kriteria as $keys => $kriterias)
                <td>
                  @if($kriterias->id == $kriteriaz->id)
                    @php 
                      $cari1 = App\Models\Ahp\EVAHPKriteria::where('kriteriaid1', $kriterias->id)->get();
                      $hasils = 1 / $cari1->sum('nilai');
                    @endphp
                    (1 / {{$cari1->sum('nilai')}}) = {{round($hasils, 2)}}
                  @else
                      @php 
                        $cekz = $ahpkriteria->where('kriteria_1_id',$kriteriaz->id)->where('kriteria_2_id',$kriterias->id)->first();
                        $ceks = $ahpkriteria->where('kriteria_1_id',$kriterias->id)->where('kriteria_2_id',$kriteriaz->id)->first();
                        $cari1 = App\Models\Ahp\EVAHPKriteria::where('kriteriaid1', $kriterias->id)->get();
                        if($cekz && $ceks){
                          $hasil1 = $cekz->ratio / $ceks->ratio;
                          $hasils = (($cekz->ratio / $ceks->ratio) / $cari1->sum('nilai'));
                        }else{
                          $hasil1 = 0;
                          $hasils = 0;
                        }
                      @endphp
                      @if($cekz && $ceks)
                        ({{round($hasil1, 2)}} / {{$cari1->sum('nilai')}}) = {{round($hasils, 2)}}
                      @else
                        0/0 = 0
                      @endif
                  @endif
                  @php 
                    $cari = App\Models\Ahp\EV2AHPKriteria::where('kriteriaid1', $kriterias->id)->where('kriteriaid2', $kriteriaz->id)->first();
                    if($cari){
                      $sav = App\Models\Ahp\EV2AHPKriteria::find($cari->id);
                      $sav->kriteriaid1 = $kriterias->id;
                      $sav->kriteriaid2 = $kriteriaz->id;
                      $sav->nilai = round($hasils, 2);
                      $sav->save();
                    }else{
                      $sav = new App\Models\Ahp\EV2AHPKriteria;
                      $sav->kriteriaid1 = $kriterias->id;
                      $sav->kriteriaid2 = $kriteriaz->id;
                      $sav->nilai = round($hasils, 2);
                      $sav->save();
                    }
                  @endphp
                </td>
              @endforeach
            </tr>
            @endforeach
            <tr>
              <td>Hasil Penjumlahan</td>
              @php 
                $max=0;
              @endphp
              @foreach($kriteria as $kriterias)
                @php 
                  $caris = App\Models\Ahp\EV2AHPKriteria::where('kriteriaid1', $kriterias->id)->get();
                  $max += $caris->sum('nilai');
                @endphp
                  <td>{{ $caris->sum('nilai') }}</td>
              @endforeach
            </tr>
            <tr>
              <td>Hasil I max</td>
                  <td>{{ $max }}</td>
            </tr>
          </tbody>
        </table>
    </div>
  </div>

  <div class="panel b-a">
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Consistency Ratio (CR)</span>          
    </div>
    <div class="panel-body" style="padding-bottom: 0px;">
        <table id="examples" class="table table-bordered m-t-none" style="width: 100%;">
          <tbody>
            <tr>
              <td>N</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>5</td>
                <td>6</td>
                <td>7</td>
                <td>8</td>
                <td>9</td>
                <td>10</td>
            </tr>
            <tr>
              <td>(RI)</td>
                <td>0</td>
                <td>0</td>
                <td>0,58</td>
                <td>0,9</td>
                <td>1,12</td>
                <td>1,24</td>
                <td>1,32</td>
                <td>1,41</td>
                <td>1,45</td>
                <td>1,49</td>
            </tr>
          </tbody>
        </table>
        Jadi untuk n={{$max}}, adalah = {{cekCR(ceil($max))}}
        <br>dapat disimpulkan untuk CR nya adalah : 
        @if(cekCR(ceil($max)) < 1) <span class="label label-success">KONSISTEN</span> @else <span class="label label-danger">TIDAK KONSISTEN</span> @endif
        <br><br><br>  
    </div>
  </div>
</div>
@endsection
@push('scripts')
    <script>
      $(document).on('click', '.save.as.page', function(e){
        $('#formData').find('input[name="status"]').val("1");
        var formDom = "formData";
        if($(this).data("form") !== undefined){
          formDom = $(this).data('form');
        }
        saveForm(formDom);
      });

      function saveForm(form="formData")
      {
        $('#' + form).find('.loading.dimmer').show();
        $("#"+form).ajaxSubmit({
          success: function(resp){
              window.location.reload();
            },
          error: function(resp){
            $('#' + form).find('.loading.dimmer').hide();
            if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
              swal(
                'Oops, Maaf!',
                resp.responseJSON.message,
                'error'
                )
            }

            $('#cover').hide();
            var response = resp.responseJSON;
            $.each(response.errors, function(index, val) {
              clearFormError(index,val);
              showFormError(index,val);
            });
            time = 5;
            interval = setInterval(function(){
              time--;
              if(time == 0){
                clearInterval(interval);
                $('.pointing.prompt.label.transition.visible').remove();
                $('.field .error-label').slideUp(500, function(e) {
                  $(this).remove();
                  $('.field.has-error').removeClass('has-error');
                  clearTimeout(interval);
                });
              }
            },1000)
          }
        });
      }
    </script>
    @yield('js-extra')
@endpush