@extends('layouts.base')

@push('css')
<link rel="stylesheet" href="#" type="text/css" />
<link rel="stylesheet" href="{{ asset('libs/assets/one-calender/simple-calendar.css') }}" type="text/css" />
@endpush

@push('js')
<script src="{{ asset('libs/assets/one-calender/jquery.simple-calendar.js') }}"></script>
<script src="http://malsup.github.com/jquery.form.js"></script> 
{{-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="https://apexcharts.com/samples/assets/stock-prices.js') }}"></script> --}}
@endpush

@section('title', 'Hasil AHP')

@section('side-header')
<div style="margin-right: 14px;">
  <span><i class="glyphicon glyphicon-home"></i></span>
</div>
@endsection

@push('styles')
@endpush

@push('scripts')
@endpush

@section('body')
<div class="row row-sm">
  <div class="panel b-a">
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Input Perbandingan Berpasangan</span>          
    </div>
    <div class="panel-body" style="padding-bottom: 0px;">
      <form action="{{ route($routes.'.simpan', 1) }}" method="POST" id="formData">
        @method('PATCH')
        @csrf
        <div class="loading dimmer padder-v" style="display: none;">
            <div class="loader"></div>
        </div>
        @foreach($kriteria as $kriterias)
          <span class="label label-info">{{$kriterias->nama}}</span><br><br>
          <table id="example" class="table table-bordered m-t-none" style="width: 100%;">
            <tbody>
              <tr>
                <td>Alternatif</td>
                @foreach($alternatif as $alternatifs)
                  <td>{{ $alternatifs->nama }}</td>
                @endforeach
              </tr>
              @foreach($alternatif as $key => $alternatifz)
              <tr>
                <td>{{ $alternatifz->nama }}</td>
                @foreach($alternatif as $keys => $alternatifs)
                  <td>
                    @if($alternatifs->id == $alternatifz->id)
                      @php 
                        $cek = $ahpalternatif->where('alternatif_1_id',$alternatifz->id)->where('alternatif_2_id',$alternatifs->id)->where('kriteria_id', $kriterias->id)->first();
                      @endphp
                      @if($cek)
                        <input type="hidden" name="detail[{{$kriterias->id}}][data][{{$key}}{{$keys}}][id]" value="{{ $cek->id }}">
                      @endif
                      <input type="hidden" name="detail[{{$kriterias->id}}][data][{{$key}}{{$keys}}][alternatif1_id]" value="{{ $alternatifz->id }}">
                      <input type="hidden" name="detail[{{$kriterias->id}}][data][{{$key}}{{$keys}}][alternatif2_id]" value="{{ $alternatifs->id }}">
                      <input type="hidden" name="detail[{{$kriterias->id}}][data][{{$key}}{{$keys}}][ratio]" value="1">
                     1/1
                    @else
                      @php 
                        $cekz = $ahpalternatif->where('alternatif_1_id',$alternatifz->id)->where('alternatif_2_id',$alternatifs->id)->where('kriteria_id', $kriterias->id)->first();
                      @endphp
                      @if($cekz)
                        <input type="hidden" name="detail[{{$kriterias->id}}][data][{{$key}}{{$keys}}][id]" value="{{ $cekz->id }}">
                      @endif
                      <input type="hidden" name="detail[{{$kriterias->id}}][data][{{$key}}{{$keys}}][alternatif1_id]" value="{{ $alternatifz->id }}">
                      <input type="hidden" name="detail[{{$kriterias->id}}][data][{{$key}}{{$keys}}][alternatif2_id]" value="{{ $alternatifs->id }}">
                      @if($cekz)
                        <select class="selectpicker form-control show-tick" data-size="3" name="detail[{{$kriterias->id}}][data][{{$key}}{{$keys}}][ratio]" data-style="btn-default" title="(Pilih Salah Satu)">
                          <option value="1" @if($cekz->ratio == 1) selected @endif>Sama penting dengan</option>
                          <option value="2" @if($cekz->ratio == 2) selected @endif>Sedikit lebih penting dari</option>
                          <option value="3" @if($cekz->ratio == 3) selected @endif>Lebih penting dari</option>
                          <option value="4" @if($cekz->ratio == 4) selected @endif>Sangat penting dari</option>
                          <option value="5" @if($cekz->ratio == 5) selected @endif>Mutlak sangat penting dari</option>
                        </select>
                      @else 
                        <select class="selectpicker form-control show-tick" data-size="3" name="detail[{{$kriterias->id}}][data][{{$key}}{{$keys}}][ratio]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                          <option value="1">Sama penting dengan</option>
                          <option value="2">Sedikit lebih penting dari</option>
                          <option value="3">Lebih penting dari</option>
                          <option value="4">Sangat penting dari</option>
                          <option value="5">Mutlak sangat penting dari</option>
                        </select>
                      @endif
                    @endif
                  </td>
                @endforeach
              </tr>
              @endforeach
            </tbody>
          </table>
        @endforeach
        <p>&nbsp;</p>
        <div class="form-row">
          <div class="form-group col-md-12">
            <div class="text-right">
              <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
              <button type="button" class="btn btn-simpan save as page">Simpan</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="panel b-a">
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Hasil Normalisasi</span>          
    </div>
    @foreach($kriteria as $kriterias)
      <span class="label label-info">{{$kriterias->nama}}</span><br><br>
      <div class="panel-body" style="padding-bottom: 0px;">
          <table id="examples" class="table table-bordered m-t-none" style="width: 100%;">
            <tbody>
              <tr>
                <td>Alternatif</td>
                @foreach($alternatif as $alternatifs)
                  <td>{{ $alternatifs->nama }}</td>
                @endforeach
              </tr>
              @foreach($alternatif as $key => $alternatifz)
              <tr>
                <td>{{ $alternatifz->nama }}</td>
                @foreach($alternatif as $keys => $alternatifs)
                  <td>
                    @if($alternatifs->id == $alternatifz->id)
                      (1/1) = 1
                        @php 
                          $hasil=1;
                        @endphp
                    @else
                        @php 
                          $cekz = $ahpalternatif->where('alternatif_1_id',$alternatifz->id)->where('alternatif_2_id',$alternatifs->id)->where('kriteria_id', $kriterias->id)->first();
                          $ceks = $ahpalternatif->where('alternatif_1_id',$alternatifs->id)->where('alternatif_2_id',$alternatifz->id)->where('kriteria_id', $kriterias->id)->first();
                          if($cekz && $ceks){
                            $hasil = ($cekz->ratio / $ceks->ratio);
                          }else{
                            $hasil =0;
                          }
                        @endphp
                        @if($cekz && $ceks)
                        ({{$cekz->ratio}} / {{$ceks->ratio}}) = {{round($hasil, 2)}}
                        @else 
                          0/0 = 0
                        @endif
                    @endif
                    @php 
                      $cari = App\Models\Ahp\EVAHPAlternatif::where('alternatifid1', $alternatifs->id)->where('alternatifid2', $alternatifz->id)->where('kriteria_id', $kriterias->id)->first();
                      if($cari){
                        $sav = App\Models\Ahp\EVAHPAlternatif::find($cari->id);
                        $sav->alternatifid1 = $alternatifs->id;
                        $sav->alternatifid2 = $alternatifz->id;
                        $sav->kriteria_id = $kriterias->id;
                        $sav->nilai = round($hasil, 2);
                        $sav->save();
                      }else{
                        $sav = new App\Models\Ahp\EVAHPAlternatif;
                        $sav->alternatifid1 = $alternatifs->id;
                        $sav->alternatifid2 = $alternatifz->id;
                        $sav->kriteria_id = $kriterias->id;
                        $sav->nilai = round($hasil, 2);
                        $sav->save();
                      }
                    @endphp
                  </td>
                @endforeach
              </tr>
              @endforeach
              <tr>
                <td>Hasil Penjumlahan</td>
                @foreach($alternatif as $alternatifs)
                  @php 
                    $cari1 = App\Models\Ahp\EVAHPAlternatif::where('alternatifid1', $alternatifs->id)->where('kriteria_id', $kriterias->id)->get();
                  @endphp
                    <td>{{ $cari1->sum('nilai') }}</td>
                @endforeach
              </tr>
            </tbody>
          </table>
      </div>
    @endforeach
  </div>

  <div class="panel b-a">
    @php
      $max=0;
    @endphp
    @foreach($kriteria as $kriteriaz)
      @php 
        $caris = App\Models\Ahp\EV2AHPKriteria::where('kriteriaid1', $kriteriaz->id)->get();
        $max += $caris->sum('nilai');
      @endphp
    @endforeach
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Hasil Nilai Alternatif</span>          
    </div>
      <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nilai I MAX Kriteria adalah = {{ $max }}</span>
      <div class="panel-body" style="padding-bottom: 0px;">
          <table id="examples" class="table table-bordered m-t-none" style="width: 100%;">
            <tbody>
              <tr>
                <td>Alternatif</td>
                @foreach($kriteria as $kriterias)
                  <td>{{ $kriterias->nama }}</td>
                @endforeach
                <td>Total</td>
              </tr>
              @foreach($alternatif as $alternatifs)
              <tr>
                <td>{{$alternatifs->nama}}</td>
                @php 
                  $tot=0;
                @endphp
                @foreach($kriteria as $kriterias)
                  <td>
                    @php 
                    $cari1 = App\Models\Ahp\EVAHPAlternatif::where('alternatifid1', $alternatifs->id)->where('kriteria_id', $kriterias->id)->get();
                    $total = $cari1->sum('nilai') / $max;
                    $tot += round($total, 3);
                    @endphp
                    {{ $cari1->sum('nilai') }} / {{ $max }} = {{ round($total, 3) }}
                  </td>
                @endforeach
                <td>{{ $tot }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
      </div>
  </div>
</div>
@endsection
@push('scripts')
    <script>
      $(document).on('click', '.save.as.page', function(e){
        $('#formData').find('input[name="status"]').val("1");
        var formDom = "formData";
        if($(this).data("form") !== undefined){
          formDom = $(this).data('form');
        }
        saveForm(formDom);
      });

      function saveForm(form="formData")
      {
        $('#' + form).find('.loading.dimmer').show();
        $("#"+form).ajaxSubmit({
          success: function(resp){
              window.location.reload();
            },
          error: function(resp){
            $('#' + form).find('.loading.dimmer').hide();
            if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
              swal(
                'Oops, Maaf!',
                resp.responseJSON.message,
                'error'
                )
            }

            $('#cover').hide();
            var response = resp.responseJSON;
            $.each(response.errors, function(index, val) {
              clearFormError(index,val);
              showFormError(index,val);
            });
            time = 5;
            interval = setInterval(function(){
              time--;
              if(time == 0){
                clearInterval(interval);
                $('.pointing.prompt.label.transition.visible').remove();
                $('.field .error-label').slideUp(500, function(e) {
                  $(this).remove();
                  $('.field.has-error').removeClass('has-error');
                  clearTimeout(interval);
                });
              }
            },1000)
          }
        });
      }
    </script>
    @yield('js-extra')
@endpush