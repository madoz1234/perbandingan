<!DOCTYPE html>
<html lang="it"><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"><title>Action Item Emails :</title><!--


COLORE INTENSE  #9C010F
COLORE LIGHT #EDE8DA

TESTO LIGHT #3F3D33
TESTO INTENSE #ffffff


--><meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<style type="text/css">
	* {
		margin: 0;
		padding: 0;
	}
	body {
		font: 14px/1.5 sans-serif;
		background: #ebe6e6;
		color: #555;
	}
	a {
		color: #297ca0;
		text-decoration: none;
	}
	.container {
		padding: 2em;
	}
	header {
		text-align: center;
		background: #297ca0;
		color: #fff;
		position: relative;
	}
	header img {
		vertical-align: middle;
		position: absolute;
		top: 50%;
		left: 2em;
		transform: translateY(-50%);
	}
	header h1 {
		font-size: 1.5em;
		margin-top: 0;
	}
	header small {
		display: block;
		font-size: 12px;
		font-weight: normal;
	}
	.btn {
		display: inline-block;
		padding: 10px 25px;
		font-size: 1.2em;
		background: #297ca0;
		color: #fff;
		text-decoration: none;
		margin-bottom: 20px;
	}
	p {
		margin-bottom: 20px;
	}
	footer {
		background: #999;
		color: #fff;
		display: flex;
	}
	footer .container {
		width: 100%;
	}
	footer .one-two {
		display: block;
		width: 50%;
		float: left;
	}
	footer:after {
		content: " ";
		display: block;
		clear: both;
	}

	.btn.hadir.a {
		display: inline-block;
		padding: 10px 25px;
		font-size: 1.2em;
		background: #21bf73;
		color: #fff;
		text-decoration: none;
		margin-bottom: 20px;
	}

	.center {
		text-align: center;
	}

	.table{
		width: 100%;
	}

	.table tr td {
		vertical-align: top;
	}

	.table .label {
		font-weight: bold;
		width: 200px;
	}

	.table .titik-dua {
		width: 5px;
		margin-left: 0;
		margin-right: 0;
	}
</style>
</head>
<body>
	<header>
		<div class="container">
			<img src="https://www.waskita.co.id/img/logo/wk.png" alt="logo" width="150px" height="100px">
			<h1>PT. Waskita Karya (Persero) Tbk.
				<small>Notifikasi: IA Online</small>
			</h1>
		</div>
	</header>
	<section>
		<div class="container">
			<table class="table">
				<tbody>
					<tr>
						<td>
							<p style="background: #3F3D33; padding: 15px; font-size: 16px;">{{ $record->keterangan }}</p>
						</td>
					</tr>
					<tr>
						<td>
							Kunjungi : <a href="{{ $record->url }}">{{ $record->url }}</a>
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
	</section>
	<footer>
		<div class="container">
			<div class="one-two" style="text-align: left">
				PT. Waskita Karya (Persero) Tbk.
			</div>
			<div class="one-two" style="text-align: right">
				<span>Waskita Building  MT Haryono Kav. No 10,<br>&emsp;&emsp;&emsp; Cawang - Jakarta 13340</span><br>
				<span>(021) 850-8510</span><br>
				<span>waskita@waskita.co.id</span>
			</div>
		</div>
	</footer>
</body>
</html>






