<!DOCTYPE html>
<html lang="en" class="">
<head>
  <meta charset="utf-8" />
  <title>{{ config('app.name') }}</title>
  <meta name="description" content="app, web app, responsive, responsive layout, admin, admin panel, admin dashboard, flat, flat ui, ui kit, AngularJS, ui route, charts, widgets, components" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  {{-- <link rel="shortcut icon" type="image/png" href="{{ asset('favicon.ico') }}"/> --}}
  <link rel="stylesheet" href="{{ asset('libs/assets/animate.css/animate.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('libs/assets/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('libs/assets/simple-line-icons/css/simple-line-icons.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('libs/jquery/bootstrap/dist/css/bootstrap.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('libs/jquery/bootstrap/dist/css/clockpicker.css') }}" type="text/css" />
  {{-- <link rel="stylesheet" href="{{ asset('libs/jquery/bootstrap/dist/css/standalone.css') }}" type="text/css" /> --}}
  <link rel="stylesheet" href="{{ asset('libs/jquery/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}" type="text/css" />

  <link rel="stylesheet" href="{{ asset('src/css/font.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('src/css/app.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('css/aside-fix.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('libs/jquery/bootstrap-toggle/bootstrap-toggle.css') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('libs/jquery/bootstrap-file-input/css/bootstrap.min.css') }}" crossorigin="anonymous">
  <link href="{{ asset('libs/jquery/bootstrap-file-input/css/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('libs/jquery/bootstrap-file-input/css/fileinput-rtl.min.css') }}" media="all" rel="stylesheet" type="text/css" />

  @stack('css')
  
  @stack('styles')
</head>
<body>

<div class="app app-header-fixed app-aside-fixed">
{{-- <div class="app app-header-fixed app-aside-folded"> --}}
  <!-- header -->
  @include('partials.header')
  <!-- / header -->

  <!-- aside -->  
  @include('partials.sidebar')
  <!-- / aside -->

  <!-- content -->
  @yield('content')
  <!-- /content -->
  
  <!-- footer -->
  @include('partials.footer')
  <!-- / footer -->
</div>

@stack('modals')

<script src="{{ asset('libs/jquery/bootstrap-file-input/js/jquery-3.3.1.min.js') }}" crossorigin="anonymous"></script>
<script src="{{ asset('libs/jquery/jquery/dist/jquery.js') }}"></script>
<script src="{{ asset('libs/jquery/bootstrap/dist/js/bootstrap.js') }}"></script>
<script src="{{ asset('libs/jquery/bootstrap/dist/js/clockpicker.js') }}"></script>
<script src="{{ asset('libs/jquery/bootstrap/dist/js/moment.min.js') }}"></script>
<script src="{{ asset('libs/jquery/moment/id.js') }}"></script>
<script src="{{ asset('libs/jquery/redirect/jquery.redirect.js') }}"></script>
{{-- <script src="{{ asset('libs/assets/chartjs/Chart.bundle.js') }}"></script>
<script src="{{ asset('libs/assets/chartjs/utils.js') }}"></script> --}}
<script src="{{ asset('src/js/ui-load.js') }}"></script>
<script src="{{ asset('src/js/ui-jp.config.js') }}"></script>
<script src="{{ asset('src/js/ui-jp.js') }}"></script>
<script src="{{ asset('src/js/ui-nav.js') }}"></script>
<script src="{{ asset('src/js/ui-toggle.js') }}"></script>
<script src="{{ asset('src/js/ui-client.js') }}"></script>
<script src="{{ asset('libs/jquery/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('libs/jquery/bootstrap-toggle/bootstrap-toggle.js') }}"></script>

<script src="{{ asset('libs/jquery/bootstrap-file-input/js/piexif.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('libs/jquery/bootstrap-file-input/js/sortable.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('libs/jquery/bootstrap-file-input/js/purify.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('libs/jquery/bootstrap-file-input/js/popper.min.js') }}"></script>
<script src="{{ asset('libs/jquery/bootstrap-file-input/locales/id.js') }}"></script>
<script src="{{ asset('libs/jquery/bootstrap-file-input/js/fileinput.min.js') }}"></script>
<script src="{{ asset('libs/jquery/bootstrap-file-input/js/theme.min.js') }}"></script -->

@stack('js')

<script>
  $(document).on('click', '.logout', function(e){
    $.redirect("{{ route('logout') }}", { _token: "{{ csrf_token() }}" })
    $("#input-b6").fileinput({
    		showUpload: false,
    		dropZoneEnabled: false,
    		maxFileCount: 10,
    		mainClass: "input-group-lg"
    	});
  });
  $(document).on('click', '.fa-dedent', function(e){
	  $('.dataTables_scrollHeadInner').css('width', '100%');
	  $('.table.table-bordered.dataTable.no-footer').css('width', '100%');
  });
</script>

@stack('scripts')

</body>
</html>
