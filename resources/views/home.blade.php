@extends('layouts.base')

@push('css')
    <link rel="stylesheet" href="#" type="text/css" />
    <link rel="stylesheet" href="{{ asset('libs/assets/one-calender/simple-calendar.css') }}" type="text/css" />
@endpush

@push('js')
    <script src="{{ asset('libs/assets/one-calender/jquery.simple-calendar.js') }}"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://apexcharts.com/samples/assets/stock-prices.js') }}"></script> --}}
@endpush

@section('title', 'Dashboard')

@section('side-header')
  <div style="margin-right: 14px;">
    <span><i class="glyphicon glyphicon-home"></i></span>
  </div>
@endsection

@push('styles')
  {{-- <link rel="stylesheet" href="{{ asset('libs/jquery/jquery-ui-1.12.1/jquery-ui.min.css') }}" type="text/css" /> --}}
    <style>
      
    </style>
@endpush

@push('scripts')
<script type="text/javascript">
  // calender
  $(document).ready(function () {
    $("#calender").simpleCalendar({
      fixedStartDay: false,
      disableEmptyDetails: true,
      events: [
        // generate new event after tomorrow for one hour
        {
          startDate: new Date(new Date().setHours(new Date().getHours() + 24)).toDateString(),
          endDate: new Date(new Date().setHours(new Date().getHours() + 25)).toISOString(),
          summary: '-Pembahasan Aplikasi Waskita-IAO'
        },
        // generate new event for yesterday at noon
        {
          startDate: new Date(new Date().setHours(new Date().getHours() - new Date().getHours() - 12, 0)).toISOString(),
          endDate: new Date(new Date().setHours(new Date().getHours() - new Date().getHours() - 11)).getTime(),
          summary: 'Pembukaan dan Arahan dari SVP Internal Audit'
        },
        // generate new event for the last two days
        {
          startDate: new Date(new Date().setHours(new Date().getHours() - 48)).toISOString(),
          endDate: new Date(new Date().setHours(new Date().getHours() - 24)).getTime(),
          summary: 'Pemaparan Workshop Probity Audit'
        }
      ],

    });
  });
  // calender
    var options = {
  chart: {
    toolbar: {
      show: false
    },
    type: "bar",
    height: 393,
    stacked: true,
    stackType: "100"
  },
  annotations: {
    position: "front",
    xaxis: [
      {
        x: 100,
        borderColor: "transparent",
        label: {
          borderColor: "transparent",
          borderWidth: 0,
          // text: "55%",
          offsetY: 40,
          orientation: "horizontal",
          textAnchor: "end"
        }
      }
    ]
  },
  colors: ["#408FFB", "#D3D3D3"],
  plotOptions: {
    bar: {
      horizontal: false,
      barHeight: "80%"
    }
  },

  legend: {
    position: 'top',
    show: true
  },

  grid: {
    show: true,
    borderColor: "#90A4AE",
    padding: {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0
    },
    yaxis: {
      lines: {
        show: false
      }
    }
  },

  states: {
    hover: {
      filter: {
        type: "none"
      }
    }
  },

  dataLabels: {
    enabled: true,
    enabledOnSeries: [1]
  },
  stroke: {
    width: 1,
    colors: ["#fff"]
  },
  series: [
    {
      name: "On Progress",
      data: [44, 55, 41, 37, 22, 53, 32, 33, 52, 13, 55]
    },
    {
      name: "Completed",
      data: [53, 32, 33, 52, 13, 55, 41, 37, 22, 53, 33]
    }
  ],
  yaxis: {
    title: {
      text: "Kasus"
    }
  },

  xaxis: {
    categories: [
      "RKIA",
      "Surat Penugasan",
      "Permintaan Dokumen",
      "Program Audit",
      "Opening Meeting",
      "KKA",
      "Closing Meeting",
      "LHA",
      "Surat Perintah TL",
      "Laporan TL",
      "Monitoring TL"
    ],

    labels: {
      // rotate: 0,
      show: true
    },
    axisBorder: {
      show: false
    },
    axisTicks: {
      show: false
    }
  },

  tooltip: {
    enabled: false
  }
};

var chart = new ApexCharts(document.querySelector("#chart"), options);

chart.render();

// chart2
var options = {
  chart: {
    toolbar: {
      show: false
    },
    type: "bar",
    height: 200,
    stacked: true,
    stackType: "100"
  },
  annotations: {
    position: "front",
    xaxis: [
      {
        x: 100,
        borderColor: "transparent",
        label: {
          borderColor: "transparent",
          borderWidth: 0,
          // text: "55%",
          offsetY: 40,
          orientation: "horizontal",
          textAnchor: "end"
        }
      }
    ]
  },
  colors: ["#408FFB", "#D3D3D3"],
  plotOptions: {
    bar: {
      horizontal: false,
      barHeight: "80%"
    }
  },

  legend: {
    position: 'top',
    show: true
  },

  grid: {
    show: true,
    borderColor: "#90A4AE",
    padding: {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0
    },
    yaxis: {
      lines: {
        show: false
      }
    }
  },

  states: {
    hover: {
      filter: {
        type: "none"
      }
    }
  },

  dataLabels: {
    enabled: true,
    enabledOnSeries: [1]
  },
  stroke: {
    width: 1,
    colors: ["#fff"]
  },
  series: [
    {
      name: "On Progress",
      data: [44, 55, 41, 37, 22, 33]
    },
    {
      name: "Completed",
      data: [53, 52, 13, 55, 41, 37]
    }
  ],

  xaxis: {
    categories: [
      "2015",
      "2016",
      "2017",
      "2018",
      "2019",
      "2020"
    ],

    labels: {
      show: true
    },
    axisBorder: {
      show: false
    },
    axisTicks: {
      show: false
    }
  },

  tooltip: {
    enabled: false
  }
};

var chart = new ApexCharts(document.querySelector("#chart2"), options);

chart.render();

// chart pie
var options = {
      chart: {
        height: 250,
        type: 'pie',
        events: {
          click: function (event, chartContext, config) {
            console.log("click");
          },
          dataPointSelection: function (event, chartContext, config) {
            console.log("dataPointSelection");
          }
        }
      },
      series: [44, 55, 13],
      labels: ['Sudah', 'Belum', 'Dalam Proses'],
      colors: ['#ff4560', '#feb019', '#00e396'],
      // responsive: [{
      //   breakpoint: 480,
      //   options: {
      //     chart: {
      //       width: 200,
      //       events: {
      //         click: function (event, chartContext, config) {
      //           console.log("test click");
      //         },
      //         dataPointSelection: function (event, chartContext, config) {
      //           console.log("test dps");
      //         }
      //       }
      //     },
      //     legend: {
      //       position: 'top',
      //       horizontalAlign: 'center'
      //     }
      //   }
      // }]

    }

    var chart = new ApexCharts(
      document.querySelector("#chart-pie"),
      options
    );

    chart.render();
</script>
@endpush

@section('body')
{{-- <div class="panel panel-default"> --}}
  {{-- <div class="row justify-content-center"> --}}
      <div class="row" style="margin: 0px 0px 20px 0px">
        <div class="col-sm-12 text-right">
          <form id="dataFilters" class="form-inline" role="form">
            <div class="form-group">
              <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
              <input type="text" class="form-control" name="tahun_dasar" id="tahun_dasar" data-toggle="years" placeholder="Pilih Tahun" value="{{ \Carbon\Carbon::now()->format('Y') }}">
            </div>
          </form>
        </div>
      </div>

      <div class="row row-sm">
        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="panel b-a">
            <div class="panel-heading text-left no-border">
              <span class="h4 m-b font-thin">Kegiatan Audit</span>          
            </div>
            <div class="wrapper b-b b-light">
              <h4 class="m-t-none text-right">
                <span class="text-2x text-lt"><font color="#23b7e5">10</font>/50</span>
              </h4>
              <div class="m-t-sm">
                <span class="text-2x text-lt">20%</span>
                <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-info" data-toggle="tooltip" data-original-title="20%" style="width: 20%"></div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="panel b-a">
            <div class="panel-heading text-left no-border">
              <span class="h4 m-b font-thin">Survey Kepuasan Auditee</span>          
            </div>
            <div class="wrapper b-b b-light">
              <h4 class="m-t-none text-right">
                <span class="text-2x text-lt"><font color="#fad733">31</font>/50</span>
              </h4>
              <div class="m-t-sm">
                <span class="text-2x text-lt">62%</span>
                <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-warning" data-toggle="tooltip" data-original-title="62%" style="width: 62%"></div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="panel b-a">
            <div class="panel-heading text-left no-border">
              <span class="h4 m-b font-thin">Kegiatan Konsultasi</span>          
            </div>
            <div class="wrapper b-b b-light">
              <h4 class="m-t-none text-right">
                <span class="text-2x text-lt"><font color="#27c24c">28</font>/40</span>
              </h4>
              <div class="m-t-sm">
                <span class="text-2x text-lt">70%</span>
                <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-success" data-toggle="tooltip" data-original-title="70%" style="width: 70%"></div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="panel b-a">
            <div class="panel-heading text-left no-border">
              <span class="h4 m-b font-thin">Kegiatan Lainnya</span>          
            </div>
            <div class="wrapper b-b b-light">
              <h4 class="m-t-none text-right">
                <span class="text-2x text-lt"><font color="#196ABF">19</font>/50</span>
              </h4>
              <div class="m-t-sm">
                <span class="text-2x text-lt">63%</span>
                <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-primary" data-toggle="tooltip" data-original-title="63%" style="width: 63%"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row row-sm">
        <div class="col-lg-8 col-md-8 col-sm-8">
          
          <div class="panel panel-default">
            <div class="panel-heading font-bold">Tahap Audit</div>
            <div class="panel-body">
              <div class="row" style="margin: 0px 0px 20px 0px">
                <div class="col-sm-12">
                  <form id="dataFilters" class="form-inline" role="form">
                    <div class="form-group">
                      <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
                      <input type="text" class="form-control" name="tahun_dasar" id="tahun_dasar" data-toggle="years" placeholder="Pilih Tahun" value="{{ \Carbon\Carbon::now()->format('Y') }}">
                    </div>
                  </form>
                </div>
              </div>
              <div class="text-center">
                <h4>Periode Tahun {{ \Carbon\Carbon::now()->format('Y') }}</h4>
              </div>
              <div id="chart"></div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading font-bold">Progress Audit</div>
            <div class="panel-body">              
              <div class="row" style="margin: 0px 0px 20px 0px">
                <div class="col-sm-12">
                  <form id="dataFilters" class="form-inline" role="form">
                    <div class="form-group">
                      <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
                      <input type="text" class="form-control" name="tahun_dasar" id="tahun_dasar" data-toggle="years" placeholder="Pilih Tahun" value="{{ \Carbon\Carbon::now()->format('Y') }}">
                    </div>
                  </form>
                </div>
              </div>
              <table class="table table-striped bg-white b-a">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th style="width: 60px">Tahun</th>
                    <th style="width: 140px">Tipe</th>
                    <th style="width: 90px">Objek Audit</th>
                    <th style="width: 90px">No AB</th>
                    <th style="width: 90px">Stage Saat Ini</th>
                    <th style="width: 90px">Stage Selanjutnya</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>2020</td>
                    <td>Business Unit</td>
                    <td>Building Divisi</td>
                    <td>-</td>
                    <td>Program Audit</td>
                    <td>Opening Meeting</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>

          <div class="col-lg-4 col-md-4 col-sm-4">
            {{-- Rapat --}}
            <div class="panel panel-default">
              <div class="panel-heading font-bold">Rapat Terdekat</div>
              <div class="panel-body">
                <div class="text-right">
                  <ul class="nav nav-pills nav-sm pull-right">
                    <li class="active"><a href="#hari" aria-controls="hari" role="tab" data-toggle="tab">Hari</a></li>
                    <li><a href="#bulan" aria-controls="bulan" role="tab" data-toggle="tab">Bulan</a></li>
                  </ul>
                </div>
                <div id="myTabContent" class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="hari">
                    <div class="col-lg-12">
                        <div class="col-lg-3">
                          <table border="0" cellpadding="0" cellspacing="0" width="100" style="background-color:#FFFFFF; border:1px solid #CCCCCC;">
                              <tr>
                                  <td align="left" valign="top" style="padding:5px;">
                                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                          <tr>
                                              <td align="center" valign="top" style="background-color:#2C9AB7; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:15px; font-weight:bold; padding-top:5px; padding-bottom:5px; text-align:center;">
                                                  {{ App\Libraries\CoreSn::Bulan(Carbon::now()) }}
                                              </td>
                                          </tr>
                                          <tr>
                                              <td align="center" valign="top" style="color:#000000; font-family:Helvetica, Arial, sans-serif; font-size:20px; font-weight:bold; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:center;">
                                                  {{ Carbon::now()->format('d') }}
                                              </td>
                                          </tr>
                                          <tr>
                                              <td align="center" valign="top" style="color:#c9d1d3; font-family:Helvetica, Arial, sans-serif; font-size:15px; font-weight:bold; text-align:center;">
                                                  {{ App\Libraries\CoreSn::Day(Carbon::now()) }}
                                              </td>
                                          </tr>
                                      </table>
                                  </td>
                              </tr>
                          </table>
                        </div>
                        <div class="col-lg-9">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" padding-top="10px;">
                              <tr>
                                  <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">Tgl</td>
                                  <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">{{ App\Libraries\CoreSn::DateToStringWDay(Carbon::now()) }}</td>
                              </tr>
                              <tr>
                                  <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">Tempat</td>
                                  <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">Jakarta</td>
                              </tr>
                              <tr>
                                  <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">Agenda</td>
                                  <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">
                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" padding-top="10px;">
                                          <tr>
                                              <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">- Pembahasan Aplikasi Waskita-IAO</td>
                                          </tr>
                                      </table>
                                  </td>
                              </tr>
                          </table>
                        </div>
                    </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="bulan">
                    <div class="col-lg-12">
                        <div id="calender" class="calendar-container" style="padding: 10px;"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {{-- <div class="panel panel-default">
              <div class="panel-heading font-bold">Rapat Terdekat</div>
              <div class="panel-body">
                  <div class="col-lg-3">
                    <table border="0" cellpadding="0" cellspacing="0" width="100" style="background-color:#FFFFFF; border:1px solid #CCCCCC;">
                        <tr>
                            <td align="left" valign="top" style="padding:5px;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" valign="top" style="background-color:#2C9AB7; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:15px; font-weight:bold; padding-top:5px; padding-bottom:5px; text-align:center;">
                                            {{ App\Libraries\CoreSn::Bulan(Carbon::now()) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" style="color:#000000; font-family:Helvetica, Arial, sans-serif; font-size:20px; font-weight:bold; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:center;">
                                            {{ Carbon::now()->format('d') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" style="color:#c9d1d3; font-family:Helvetica, Arial, sans-serif; font-size:15px; font-weight:bold; text-align:center;">
                                            {{ App\Libraries\CoreSn::Day(Carbon::now()) }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                  </div>
                  <div class="col-lg-9">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" padding-top="10px;">
                        <tr>
                            <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">Tgl</td>
                            <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">{{ App\Libraries\CoreSn::DateToStringWDay(Carbon::now()) }}</td>
                        </tr>
                        <tr>
                            <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">Tempat</td>
                            <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">Jakarta</td>
                        </tr>
                        <tr>
                            <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">Agenda</td>
                            <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" padding-top="10px;">
                                    <tr>
                                        <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">- Pembahasan Aplikasi Waskita-IAO</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                  </div>
              </div>
            </div> --}}
            {{-- Kegiatan Internal Audit --}}
              <div class="panel panel-default">
                <div class="panel-heading font-bold">Kegiatan Internal Audit</div>
                <div class="panel-body">
                  <div class="row" style="margin: 0px 0px 20px 0px">
                    <div class="col-sm-12">
                      <form id="dataFilters" class="form-inline" role="form">
                        <div class="form-group">
                          <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
                          <input type="text" class="form-control" name="tahun_dasar" id="tahun_dasar" data-toggle="years" placeholder="Pilih Tahun" value="2015">
                          <input type="text" class="form-control" name="tahun_dasar" id="tahun_dasar" data-toggle="years" placeholder="Pilih Tahun" value="{{ \Carbon\Carbon::now()->format('Y') }}">
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="text-center">
                    <h4>Periode Tahun 2015 - {{ \Carbon\Carbon::now()->format('Y') }}</h4>
                  </div>
                  <div id="chart2"></div>
                </div>
              </div>


            {{-- Chart Pie --}}
            <div class="panel panel-default">
              <div class="panel-heading font-bold">Jumlah Temuan</div>
              <div class="panel-body">
                <h4 class="pull-right">Periode {{ \Carbon\Carbon::now()->format('Y') }}</h4>
                <form id="dataFilters" class="form-inline" role="form">
                  <div class="form-group">
                    <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
                    <input type="text" class="form-control" name="tahun_dasar" id="tahun_dasar" data-toggle="years" placeholder="Pilih Tahun" value="{{ \Carbon\Carbon::now()->format('Y') }}">
                  </div>
                </form>
                  <div id="chart-pie"></div>
              </div>
            </div>

            </div>
          </div>
      </div>

  {{-- </div> --}}
{{-- </div> --}}
@endsection