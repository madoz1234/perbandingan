<?php

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/home', function(){
  return redirect()->route('dashboard.home.index');
});

Route::middleware('auth')->group(function() {
	Route::name('dashboard.')->prefix('dashboard')->namespace('Dashboard')->group( function() {
		Route::get('/', function(){
			return redirect()->route('dashboard.home.index');
		});
		Route::resource('home', 'HomeController');
	});
	Route::name('setting.')->prefix('setting')->namespace('Setting')->group( function() {
		Route::get('/', function(){
			return redirect()->route('setting.users.index');
		});

		Route::post('users/grid', 'UserController@grid')->name('users.grid');
		Route::resource('users', 'UserController');

		Route::post('log/grid', 'LogController@grid')->name('log.grid');
		Route::resource('log', 'LogController');

		Route::post('roles/grid', 'RoleController@grid')->name('roles.grid');
		Route::patch('roles/saveData/{id}','RoleController@saveData')->name('roles.saveData');
		Route::resource('roles', 'RoleController');
	});
	Route::name('topsis.')->prefix('topsis')->namespace('Topsis')->group( function() {
        Route::post('nilai/grid', 'NilaiAlternatifController@grid')->name('nilai.grid');
        Route::get('nilai/{id}/detail','NilaiAlternatifController@detail')->name('nilai.detail');
		Route::resource('nilai', 'NilaiAlternatifController');

		Route::post('hasil/index', 'HasilTopsisController@index')->name('hasil.index');
		Route::resource('hasil', 'HasilTopsisController');
	});

	Route::name('ahp.')->prefix('ahp')->namespace('Ahp')->group( function() {
		Route::post('kriteria/index', 'AHPKriteriaController@index')->name('kriteria.index');
		Route::patch('kriteria/{id}/simpan','AHPKriteriaController@simpan')->name('kriteria.simpan');
		Route::resource('kriteria', 'AHPKriteriaController');

		Route::post('alternatif/index', 'AHPAlternatifController@index')->name('alternatif.index');
		Route::patch('alternatif/{id}/simpan','AHPAlternatifController@simpan')->name('alternatif.simpan');
		Route::resource('alternatif', 'AHPAlternatifController');
	});

	Route::name('master.')->prefix('master')->namespace('Master')->group( function() {
        Route::post('kriteria/grid', 'KriteriaController@grid')->name('kriteria.grid');
		Route::resource('kriteria', 'KriteriaController');

		Route::post('alternatif/grid', 'AlternatifController@grid')->name('alternatif.grid');
		Route::resource('alternatif', 'AlternatifController');
	});
});

Auth::routes();
