<?php

namespace App\Http\Requests\Topsis;

use App\Http\Requests\FormRequest;

class NilaiAlternatifRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'alternatif_id'            			=> 'required',
            'detail.*.nilai'                    => 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'alternatif_id.required'        => 'Alternatif tidak boleh kosong',
            'detail.*.nilai.required'       => 'Nilai tidak boleh kosong',
       ];
    }
}
