<?php

namespace App\Http\Requests\Setting;

use App\Http\Requests\FormRequest;

class UserRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   	public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
		if($input['tipe'] == 1){
			if($input['roles'] == 10){
				if($input['kategori'] == 1){
	    			$return = [
			            'nip'   		=> 'required|max:12|unique:sys_users,nip,'.request()->id,
			            'name'   		=> 'required|max:255',
			            'email'   		=> 'required|max:255|email|unique:sys_users,email,'.request()->id,
			            'roles'   		=> 'required',
			            'tipe'   		=> 'required',
			            'fungsi'   		=> 'required',
			            'bu_id'   		=> 'required',
			            'kategori'   	=> 'required',
			            'inisial'   	=> 'required|max:4|unique:sys_users,inisial,'.request()->id,
			        ];
				}else{
					$return = [
			            'nip'   		=> 'required|max:12|unique:sys_users,nip,'.request()->id,
			            'name'   		=> 'required|max:255',
			            'email'   		=> 'required|max:255|email|unique:sys_users,email,'.request()->id,
			            'roles'   		=> 'required',
			            'tipe'   		=> 'required',
			            'fungsi'   		=> 'required',
			            'co_id'   		=> 'required',
			            'kategori'   	=> 'required',
			            'inisial'   	=> 'required|max:4|unique:sys_users,inisial,'.request()->id,
			        ];
				}
			}else{
				if($input['kategori'] == 1){
    				$return = [
			            'nip'   		=> 'required|max:12|unique:sys_users,nip,'.request()->id,
			            'name'   		=> 'required|max:255',
			            'email'   		=> 'required|max:255|email|unique:sys_users,email,'.request()->id,
			            'roles'   		=> 'required',
			            'tipe'   		=> 'required',
			            'bu_id'   		=> 'required',
			            'kategori'   	=> 'required',
			        ];
				}else{
					$return = [
			            'nip'   		=> 'required|max:12|unique:sys_users,nip,'.request()->id,
			            'name'   		=> 'required|max:255',
			            'email'   		=> 'required|max:255|email|unique:sys_users,email,'.request()->id,
			            'roles'   		=> 'required',
			            'tipe'   		=> 'required',
			            'co_id'   		=> 'required',
			            'kategori'   	=> 'required',
			        ];
				}
			}
		}else{
			if($input['kategori'] == 1){
				$return = [
		            'nip'   		=> 'required|max:12|unique:sys_users,nip,'.request()->id,
		            'name'   		=> 'required|max:255',
		            'email'   		=> 'required|max:255|email|unique:sys_users,email,'.request()->id,
		            'role'   		=> 'required',
		            'tipe'   		=> 'required',
		            'bu_id'   		=> 'required',
		            'kategori'   	=> 'required',
		        ];
			}else{
				$return = [
		            'nip'   		=> 'required|max:12|unique:sys_users,nip,'.request()->id,
		            'name'   		=> 'required|max:255',
		            'email'   		=> 'required|max:255|email|unique:sys_users,email,'.request()->id,
		            'role'   		=> 'required',
		            'tipe'   		=> 'required',
		            'co_id'   		=> 'required',
		            'kategori'   	=> 'required',
		        ];
			}
		}
		return $return;
    }

    public function messages()
    {
    	return [
        	'kategori.required'     => 'Kategori tidak boleh kosong',
        	'bu_id.required'      	=> 'Business Unit (BU) tidak boleh kosong',
        	'co_id.required'      	=> 'Corporate Office (CO) tidak boleh kosong',
        	'name.required'      	=> 'Nama tidak boleh kosong',
        	'name.max'       	  	=> 'Maks Karakter (255)',
        	'nip.required'      	=> 'NIP tidak boleh kosong',
        	'nip.max'       	  	=> 'Maks Karakter (12)',
        	'nip.unique'       	  	=> 'NIP sudah ada',
        	'inisial.required'      => 'Inisial tidak boleh kosong',
        	'inisial.max'       	=> 'Maks Karakter (4)',
        	'inisial.unique'       	=> 'Inisial sudah ada',
        	'email.required'      	=> 'Email tidak boleh kosong',
        	'password.required'   	=> 'Password tidak boleh kosong',
        	'tipe.required'   		=> 'Tipe tidak boleh kosong',
        	'role.required'   		=> 'Role tidak boleh kosong',
        	'roles.required'   		=> 'Role tidak boleh kosong',
        	'fungsi.required'  		=> 'Fungsi tidak boleh kosong',
        	'password.min'  		=> 'Min Karakter (6)',
        	'password.confirmed'  	=> 'Password dan Konfirmasi Password tidak sama',
       ];
    }
}
