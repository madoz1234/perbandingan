<?php

namespace App\Http\Controllers\Topsis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\CoreSn;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Master\Alternatif;
use App\Models\Master\Kriteria;
use App\Models\Topsis\NilaiAlternatif;
use App\Models\Topsis\NilaiAlternatifDetail;
use Carbon\Carbon;

class HasilTopsisController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $routes = 'topsis.hasil';
    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $nilai = NilaiAlternatif::get();
        $kriteria = Kriteria::get();
        $alternatif = Alternatif::get();
        $nilaidetail = NilaiAlternatifDetail::get();
        return $this->render('modules.topsis.hasil', [
            'mockup' => true,
            'kriteria' => $kriteria,
            'alternatif' => $alternatif,
            'nilai' => $nilai,
            'nilaidetail' => $nilaidetail,
        ]);
    }
}
