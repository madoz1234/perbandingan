<?php

namespace App\Http\Controllers\Topsis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Topsis\NilaiAlternatifRequest;
use App\Models\Master\Alternatif;
use App\Models\Master\Kriteria;
use App\Models\Topsis\NilaiAlternatif;

use DB;

class NilaiAlternatifController extends Controller
{
    protected $routes = 'topsis.nilai';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Topsis' => '#', 'Nilai Alternatif' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Alternatif',
                'className' => 'text-center',
                'width' => '300px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = NilaiAlternatif::select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($name = request()->name) {
            $records->where('nama', 'like', '%' . $name . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('nama', function ($record) {
                   return $record->alternatif->nama;
               })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';
                   $buttons .= $this->makeButtons([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-eye text-primary"></i>',
                        'tooltip'   => 'Detail',
                        'class'     => 'detail button',
                        'id'        => $record->id,
                    ]);
                   $buttons .='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                   $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                   ]);
                   return $buttons;
               })
               ->rawColumns(['status', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.topsis.nilai-alternatif.index');
    }

    public function create()
    {
        $kriteria = Kriteria::get();
        return $this->render('modules.topsis.nilai-alternatif.create', ['kriteria' => $kriteria]);
    }

    public function store(NilaiAlternatifRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new NilaiAlternatif;
			$record->alternatif_id 	= $request->alternatif_id;
	        $record->save();
            $record->saveDetail($request->detail);

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function edit(NilaiAlternatif $nilai)
    {
        $kriteria = Kriteria::get();
        return $this->render('modules.topsis.nilai-alternatif.edit', [
            'record' => $nilai,
            'kriteria' => $kriteria
        ]);
    }

    public function detail(NilaiAlternatif $id)
    {
       $kriteria = Kriteria::get();
        return $this->render('modules.topsis.nilai-alternatif.detail', [
            'record' => $id,
            'kriteria' => $kriteria
        ]);
    }

    public function update(NilaiAlternatifRequest $request, NilaiAlternatif $nilai)
    {
    	DB::beginTransaction();
        try {
        	$record = NilaiAlternatif::find($request->id);
			$record->alternatif_id 		= $request->alternatif_id;
	        $record->updateDetail($request->detail);

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Alternatif $alternatif)
    {
        if($alternatif){
    		
    	}else{
    		return response([
                'status' => true,
            ],500);
    	}
    }
}
