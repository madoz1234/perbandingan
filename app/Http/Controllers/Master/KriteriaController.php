<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\KriteriaRequest;
use App\Models\Master\Kriteria;
use App\Models\SessionLog;

use DB;

class KriteriaController extends Controller
{
    protected $routes = 'master.kriteria';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Kriteria' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'kode',
                'name' => 'kode',
                'label' => 'Kode Kriteria',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Kriteria',
                'width' => '250px',
                'sortable' => true,
            ],
            [
                'data' => 'bobot',
                'name' => 'bobot',
                'label' => 'Bobot %',
                'className' => 'text-center',
                'width' => '250px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Kriteria::select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($kode = request()->kode) {
            $records->where('kode', 'like', '%' . $kode . '%');
        }
        if ($name = request()->name) {
            $records->where('nama', 'like', '%' . $name . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('kode', function ($record) {
                   if($record->kode){
	           			return $record->kode;
	           		}else{
	                   return '';
	           		}
               })
               ->addColumn('nama', function ($record) {
                   return $record->nama;
               })
               ->addColumn('bobot', function ($record) {
                   return $record->bobot;
               })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';

                   $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                   ]);
                   return $buttons;
               })
               ->rawColumns(['alamat', 'action','pic'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.kriteria.index');
    }

    public function create()
    {
        return $this->render('modules.master.kriteria.create');
    }

    public function store(KriteriaRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new Kriteria;
	        $record->fill($request->all());
	        $record->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(BU $user)
    // {
    //     return $user->toJson();
    // }

    public function edit($data)
    {
    	$kriteria = Kriteria::find($data);
        return $this->render('modules.master.kriteria.edit', ['record' => $kriteria]);
    }

    public function update(KriteriaRequest $request, Kriteria $kriteria)
    {
    	DB::beginTransaction();
        try {
        	$record = Kriteria::find($request->id);
	        $record->fill($request->all());
	        $record->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Kriteria $kriteria)
    {
        if($kriteria){
    		
    	}else{
    		return response([
                'status' => true,
            ],500);
    	}
    }
}
