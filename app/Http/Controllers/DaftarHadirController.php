<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Rapat\RapatInternal;
use App\Models\Rapat\DaftarHadir;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Libraries\CoreSn;
use App\Models\SessionLog;

class DaftarHadirController extends Controller
{
    protected $routes = 'ex';
    public function __construct()
    {
        $this->setRoutes($this->routes);
    }

    public function showing($id,$real)
    {
        if(md5(base64_decode($real)) == $id){
            $record = RapatInternal::findOrFail(base64_decode($real));
            if(Carbon::now()->format('d-m-Y') == CoreSn::DateToSql($record->tanggal)){
                $jam = Carbon::createFromFormat('d-m-Y H:i',$record->tanggal.' '.$record->jam_mulai);
                $jamnow = Carbon::now();
                // $diff = abs($jamnow - $jam);
                $convert= Carbon::parse($jam->format('H:i:s'))->floatDiffInHours($jamnow->format('H:i:s'), false); 
                if($convert > -1){
                    $record = RapatInternal::findOrFail(base64_decode($real));
                    return $this->render('daftar-hadir.index',[
                        'record' => $record
                    ]);
                }
            }
        }
        return abort(404);
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'email' => 'required|email',
            'nomor' => 'required',
            'perusahaan' => 'required',
        ],[
            'nama.required' => 'Tidak Boleh Kosong ',
            'email.required' => 'Tidak Boleh Kosong ',
            'email.email' => 'Email tidak valid ',
            'nomor.required' => 'Tidak Boleh Kosong ',
            'perusahaan.required' => 'Tidak Boleh Kosong ',
        ]);

        try {
            // dd($request->all());
            $cek = DaftarHadir::where('rapat_id', $request->rapat_id)
                                ->where('nama', $request->nama)
                                ->where('email', $request->email)
                                ->first();
                if (!$cek) {
                    // dd($cek);
                    $save = new DaftarHadir;
                    $save->fill($request->all());
                    $save->save();
                    $rapat = RapatInternal::find($request->rapat_id);
                    $rapat->fill(['jumlah_peserta' => $rapat->daftarHadir->count()]);
                    $rapat->save();
                }

            return response([
                'status' => true,
            ]);
        } catch (Exception $e) {
              return response([
                'status' => false,
                'errors' => $e
            ]);
        }
    }
}
