<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Rapat\RapatInternal;
use App\Models\Rapat\DaftarHadir;
use App\Models\Rapat\UndanganPesertaInternal;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Libraries\CoreSn;
use App\Models\SessionLog;

class DaftarHadirInternalController extends Controller
{
    protected $routes = 'in';
    public function __construct()
    {
        $this->setRoutes($this->routes);
    }

    public function showing($id,$real)
    {
        // dd($real);
        if(md5(base64_decode($real)) == $id){
            $record = RapatInternal::findOrFail(base64_decode($real));
            if(Carbon::now()->format('d-m-Y') == CoreSn::DateToSql($record->tanggal)){
                $jam = Carbon::createFromFormat('d-m-Y H:i',$record->tanggal.' '.$record->jam_mulai);
                $jamnow = Carbon::now();
                // $diff = abs($jamnow - $jam);
                $convert= Carbon::parse($jam->format('H:i:s'))->floatDiffInHours($jamnow->format('H:i:s'), false); 
                if($convert > -1){
                    if($record->daftarHadir->where('user_id', auth()->user()->id)->count() == 0){
                        $save = new DaftarHadir;
                        $save->fill([
                            'rapat_id' => base64_decode($real),
                            'user_id' => auth()->user()->id
                        ]);
                        $save->save();
                        // if($save->save()){
                            $undangan = UndanganPesertaInternal::where('rapat_id', base64_decode($real))
                                                                ->where('user_id', auth()->user()->id)
                                                                ->first();
                            if ($undangan) { 
                                $undangan->status    = 2;
                                $undangan->save();
                            }
                        // dd($save);
                        // }
        
                        $record = RapatInternal::findOrFail(base64_decode($real));
                        $record->fill(['jumlah_peserta' => $record->daftarHadir->count()]);
                        $record->save();
                                    
                        return $this->render('daftar-hadir-internal.index',[
                            'record' => $record
                        ]);
                    }else{
                        return $this->render('daftar-hadir-internal.index',[
                            'record' => $record
                        ]);
                    }
                }
            }
        }
        return abort(404);
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'email' => 'required',
            'nomor' => 'required',
            'perusahaan' => 'required',
        ],[
            'nama.required' => 'Tidak Boleh Kosong ',
            'email.required' => 'Tidak Boleh Kosong ',
            'nomor.required' => 'Tidak Boleh Kosong ',
            'perusahaan.required' => 'Tidak Boleh Kosong ',
        ]);
        try {
            $save = new DaftarHadir;
            $save->fill($request->all());
            $save->save();
            $rapat = RapatInternal::find($request->rapat_id);
            $rapat->fill(['jumlah_peserta' => $rapat->daftarHadir->count()]);
            $rapat->save();

            return response([
                'status' => true,
            ]);
        } catch (Exception $e) {
              return response([
                'status' => false,
                'errors' => $e
            ]);
        }
    }
}
