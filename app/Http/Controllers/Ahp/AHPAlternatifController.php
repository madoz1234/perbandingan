<?php

namespace App\Http\Controllers\Ahp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\CoreSn;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Master\Alternatif;
use App\Models\Master\Kriteria;
use App\Models\Ahp\AHPAlternatif;
use Carbon\Carbon;
use DB;

class AHPAlternatifController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $routes = 'ahp.alternatif';
    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $kriteria = Kriteria::get();
        $alternatif = Alternatif::get();
        $ahpalternatif = AHPAlternatif::get();
        return $this->render('modules.ahp.alternatif', [
            'mockup' => true,
            'kriteria' => $kriteria,
            'alternatif' => $alternatif,
            'ahpalternatif' => $ahpalternatif,
        ]);
    }

    public function simpan(Request $request)
    {
        DB::beginTransaction();
        try {
            if($request->detail){
                foreach($request->detail as $key => $datas){
                    foreach($datas['data'] as $b => $value){
                        if(!empty($value['id'])){
                            $cari = AHPAlternatif::where('kriteria_id', $key)->where('id', $value['id'])->first();
                            if($cari){
                                $newz = AHPAlternatif::find($cari->id);
                                $newz->alternatif_1_id    = $value['alternatif1_id'];
                                $newz->alternatif_2_id    = $value['alternatif2_id'];
                                $newz->kriteria_id        = $key;
                                $newz->ratio              = $value['ratio'];
                                $newz->save();
                            }
                        }else{
                            $news = new AHPAlternatif;
                            $news->alternatif_1_id    = $value['alternatif1_id'];
                            $news->alternatif_2_id    = $value['alternatif2_id'];
                            $news->kriteria_id        = $key;
                            $news->ratio              = $value['ratio'];
                            $news->save();
                        }
                    }
                }
            }

            DB::commit();
            return response([
              'status' => true
            ]); 
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }
}
