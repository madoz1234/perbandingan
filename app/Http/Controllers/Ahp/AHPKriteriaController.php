<?php

namespace App\Http\Controllers\Ahp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\CoreSn;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Master\Alternatif;
use App\Models\Master\Kriteria;
use App\Models\Topsis\NilaiAlternatif;
use App\Models\Topsis\NilaiAlternatifDetail;
use App\Models\Ahp\AHPKriteria;
use Carbon\Carbon;
use DB;

class AHPKriteriaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $routes = 'ahp.kriteria';
    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $nilai = NilaiAlternatif::get();
        $kriteria = Kriteria::get();
        $alternatif = Alternatif::get();
        $nilaidetail = NilaiAlternatifDetail::get();
        $ahpkriteria = AHPKriteria::get();
        return $this->render('modules.ahp.kriteria', [
            'mockup' => true,
            'kriteria' => $kriteria,
            'alternatif' => $alternatif,
            'nilai' => $nilai,
            'nilaidetail' => $nilaidetail,
            'ahpkriteria' => $ahpkriteria,
        ]);
    }

    public function simpan(Request $request)
    {
        DB::beginTransaction();
        try {
            if($request->detail){
                foreach($request->detail as $datas){
                    if(!empty($datas['id'])){
                        $newz = AHPKriteria::find($datas['id']);
                        $newz->kriteria_1_id    = $datas['kriteria1_id'];
                        $newz->kriteria_2_id    = $datas['kriteria2_id'];
                        $newz->ratio            = $datas['ratio'];
                        $newz->save();
                    }else{
                        $news = new AHPKriteria;
                        $news->kriteria_1_id    = $datas['kriteria1_id'];
                        $news->kriteria_2_id    = $datas['kriteria2_id'];
                        $news->ratio            = $datas['ratio'];
                        $news->save();
                    }
                }
            }

            DB::commit();
            return response([
              'status' => true
            ]); 
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }
}
