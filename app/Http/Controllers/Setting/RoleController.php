<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\Setting\RoleRequest;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\SessionLog;

class RoleController extends Controller
{
    protected $routes = 'setting.roles';
    protected $perms = 'accident-report';


    public function __construct()
    {
        $this->setRoutes($this->routes);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'name',
                'name' => 'name',
                'label' => 'Role',
                'sortable' => true,
            ],
            [
                'data' => 'users',
                'name' => 'users',
                'label' => 'Jumlah User',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'perms',
                'name' => 'perms',
                'label' => 'Permissions',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'updated_at',
                'name' => 'updated_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '90px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Role::with('users', 'permissions')->when($name = request()->name, function ($q) use ($name) {
            $q->where('name', 'like', '%'.$name.'%');
        })
                        ->select('*');
        $routes = $this->routes;

        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('perms', function ($record) {
                   return $record->permissions->count().' Permissions';
               })
               ->addColumn('users', function ($record) {
                   return $record->users->count().' Users';
               })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->editColumn('updated_at', function ($record) {
                   return $record->updated_at->diffForHumans();
               })
               ->addColumn('action', function ($record) use ($routes) {
                   $buttons = '';

                   $buttons .= $this->makeButton([
                        'type'    => 'url',
                        'class'   => auth()->user()->can($this->perms.'-edit') ? '' : 'm-r',
                        'label'   => '<i class="fa fa-check-square text-success"></i>',
                        'tooltip' => 'Assign Permissions',
                        'url'  => route($routes.'.show', $record->id),
                    ]);
				   if($record->users()->count() <= 0){
	                   if (!auth()->user()->can($this->perms.'-edit')) {
		                    $buttons .= $this->makeButton([
	                            'type' => 'edit',
	                            'id'   => $record->id,
	                        ]);
	                        $buttons .= $this->makeButton([
	                            'type' => 'delete',
	                            'id'   => $record->id,
	                        ]);
	                   }
	                }else{
	                	if (!auth()->user()->can($this->perms.'-edit')) {
	                        $buttons .= $this->makeButton([
	                            'type' => 'delete',
	                            'id'   => $record->id,
	                        ]);
	                   }
	                }

                   return $buttons;
               })
               ->make(true);
    }

    public function index()
    {
        return $this->render('settings.role.index');
    }

    public function create()
    {
        return $this->render('settings.role.create');
    }

    public function store(RoleRequest $request)
    {	
    	$user = auth()->user();
    	$log = new SessionLog;
	    $log->modul = 'Manajemen Pengguna';
	    $log->sub = 'Role';
	    $log->aktivitas = 'Membuat Role baru dengan nama '.request('name');
	    $log->user_id = $user->id;
	    $log->save();
        return Role::create(request(['name']));
    }

    public function show(Role $role)
    {
        return $this->render('settings.role.detail', ['record' => $role]);
    }

    public function edit(Role $role)
    {
        return $this->render('settings.role.edit', ['record' => $role]);
    }

    public function update(Role $role)
    {
        foreach (request()->check as $key => $value) {
            $temp = [
                'name' => $value,
            ];
            if(Permission::where('name', $value)->count() == 0){
                Permission::create($temp);
            }
        }
        $permsi = Permission::whereIn('name', request()->check)->get()->pluck('id');
        $record = Role::findById(request()->id);
        $record->permissions()->sync($permsi);
        app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();
        
        return response([
            'status' => true
        ]);
    }

    public function saveData(RoleRequest $request)
    {
    	$record = Role::find($request->id);
    	$name = $record->name;
        $record->name = $request->name;
        $record->save();

    	$user = auth()->user();
    	$log = new SessionLog;
	    $log->modul = 'Manajemen Pengguna';
	    $log->sub = 'Role';
	    $log->aktivitas = 'Mengubah '.$name.' menjadi '.$request->name;
	    $log->user_id = $user->id;
	    $log->save();
        return response([
            'status' => true
        ]);
    }

    public function destroy(Role $role)
    {
    	if($role->users()->count() > 0){
    		return response([
                'status' => 500,
            ],500);
    	}else{
    		$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Manajemen Pengguna';
		    $log->sub = 'Role';
		    $log->aktivitas = 'Menghapus Role '.$role->name;
		    $log->user_id = $user->id;
		    $log->save();

	        $role->delete();
	        return response()->json([
	            'success' => true
	        ]);
    	}

    }
}
