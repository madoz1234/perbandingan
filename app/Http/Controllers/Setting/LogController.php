<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Setting\UserRequest;
use App\Models\SessionLog;

class LogController extends Controller
{
    protected $routes = 'setting.log';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            [
                'data' => 'modul',
                'name' => 'modul',
                'label' => 'Modul',
                'sortable' => true,
                'width' => '150px',
            ],
            [
                'data' => 'sub',
                'name' => 'sub',
                'label' => 'Sub Modul',
                'sortable' => true,
                'width' => '150px',
            ],
            [
                'data' => 'aktivitas',
                'name' => 'aktivitas',
                'label' => 'Aktivitas',
                'sortable' => true,
                'width' => '400px',
            ],
            [
                'data' => 'user',
                'name' => 'user',
                'label' => 'User',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '150px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '150px',
            ],
        ]);
    }

    public function grid()
    {
        $records = SessionLog::select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
        }
        if ($modul = request()->modul) {
            $records->where('modul', 'like', '%' . $modul . '%');
        }
        if($sub = request()->sub) {
            $records->where('sub', 'like', '%' . $sub . '%');
        }
        if($user = request()->user) {
        	$records->whereHas('user', function($u) use ($user){
	            $u->where('name', 'like', '%' . $user . '%');
        	});
        }
        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->editColumn('created_at', function ($record) {
               return $record->created_at->diffForHumans();
           })
           ->addColumn('aktivitas', function ($record) {
               return $record->readMoreText('aktivitas', 200);
           })
           ->addColumn('user', function ($record) {
        		return $record->creatorName();
           })
           ->rawColumns(['fungsi', 'action','aktivitas'])
           ->make(true);
    }

    public function index()
    {
        return $this->render('settings.log.index');
    }
}
