<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Setting\UserRequest;
use App\Models\Auths\User;
use App\Models\Master\ProjectDetailPic;
use App\Models\Master\AnakPerusahaanDetailPic;
use App\Models\Master\BUDetailPic;
use App\Models\Master\CODetailPic;
use App\Models\Master\VendorDetailPic;
use App\Models\SessionLog;

class UserController extends Controller
{
    protected $routes = 'setting.users';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'name',
                'name' => 'name',
                'label' => 'Nama',
                'sortable' => true,
                'width' => '170px',
            ],
            [
                'data' => 'nip',
                'name' => 'nip',
                'label' => 'NIP',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '80px',
            ],
            [
                'data' => 'email',
                'name' => 'email',
                'label' => 'Email',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'phone',
                'name' => 'phone',
                'label' => 'No Telp / HP',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'tipe',
                'name' => 'tipe',
                'label' => 'Tipe',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '60px',
            ],
            [
                'data' => 'posisi',
                'name' => 'posisi',
                'label' => 'Role',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '60px',
            ],
            [
                'data' => 'kategori',
                'name' => 'kategori',
                'label' => 'Divisi',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '100px',
            ],
            [
                'data' => 'kategori_id',
                'name' => 'kategori_id',
                'label' => 'Lokasi',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '150px',
            ],
            [
                'data' => 'fungsi',
                'name' => 'fungsi',
                'label' => 'Fungsional',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '50px',
            ],
            [
                'data' => 'inisial',
                'name' => 'inisial',
                'label' => 'Inisial',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '30px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'updated_at',
                'name' => 'updated_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '70px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = User::select('*');
        if($name = request()->name) {
            $records->where('name', 'like', '%' . $name . '%');
        }
        if($email = request()->email) {
            $records->where('email', 'like', '%' . $email . '%');
        }
        if($fungsi = request()->fungsi) {
            $records->where('fungsi', 'like', '%' . $fungsi . '%');
        }
        if($role = request()->role) {
            $records->whereHas('roles', function($u) use($role){
            	$u->where('id', $role);
            });
        }
        if($divisi = request()->divisi) {
            $divisi = explode('-', $divisi);
            $records->whereHas($divisi[0], function($u) use($divisi){
                $u->where('id', $divisi[1]);
            });
        }
        return DataTables::of($records)
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->editColumn('nip', function ($record) {
                   return $record->nip;
               })
               ->editColumn('tipe', function ($record) {
                   $tipe = '';
	                if($record->tipe == 1){
	               	  $tipe = '<span class="label label-info">Pegawai</span>';
	                }else{
	               	  $tipe = '<span class="label label-default">Non Pegawai</span>';
	                }
	                return $tipe;
               })
               ->editColumn('kategori', function ($record) {
                   $kategori = '';

	                if($record->kategori == 1){
	               	  $kategori = 'Business Unit (BU)';
	                }else if($record->kategori == 2){
	               	  $kategori = 'Corporate Office (CO)';

	                }

	                return $kategori;
               })
               ->editColumn('phone', function ($record) {
	                return $record->phone;
               })
               ->editColumn('kategori_id', function ($record) {
	               	if($record->kategori == 1){
	               		return $record->bu ? $record->bu->nama : '';
	               	}else if($record->kategori == 2){
	               		return $record->co ? $record->co->nama : '';
	               	}
               })
               ->editColumn('inisial', function ($record) {
               		return $record->inisial;
               })
               ->addColumn('role', function ($record) {
                   return $record->roles->first()
                         ? $record->roles->first()->name
                         : '-';
               })
               ->addColumn('fungsi', function ($record) {
                    $fungsi = '';
	                if($record->fungsi == 1){
	               	  $fungsi = '<span class="label label-success">Operasional</span>';
	                }elseif($record->fungsi == 2){
	               	  $fungsi = '<span class="label label-info">Keuangan</span>';
	                }elseif($record->fungsi == 3){
	               	  $fungsi = '<span class="label label-default">Sistem</span>';
	                }else{
	               	  $fungsi = '';
	                }
	                return $fungsi;
               })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->editColumn('updated_at', function ($record) {
                   return $record->updated_at->diffForHumans();
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';

                   $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                    ]);
                   if ($record->id !== auth()->user()->id) {
                       $buttons .= $this->makeButton([
                            'type' => 'delete',
                            'id'   => $record->id,
                        ]);
                   }

                   return $buttons;
               })
               ->rawColumns(['fungsi', 'action','tipe'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('settings.user.index');
    }

    public function create()
    {
        return $this->render('settings.user.create');
    }

    public function store(UserRequest $request)
    {
    	if($request['tipe']== 1){
    		if($request['roles'] == 10){
    			if($request['kategori'] == 1){
    				$user = User::create([
	    				'nip' => $request['nip'],
	    				'name' => $request['name'],
			            'email' => $request['email'],
			            'phone' => $request['phone'],
			            'password' => Hash::make($request['password']),
		                'fungsi' => $request['fungsi'],
			            'tipe' => $request['tipe'],
			            'kategori' => $request['kategori'],
			            'kategori_id' => $request['bu_id'],
			            'inisial' => $request['inisial'],
			        ]);
	    		}else{
	    			$user = User::create([
		    			'nip' => $request['nip'],
	    				'name' => $request['name'],
			            'email' => $request['email'],
			            'phone' => $request['phone'],
			            'password' => Hash::make($request['password']),
		                'fungsi' => $request['fungsi'],
			            'tipe' => $request['tipe'],
			            'kategori' => $request['kategori'],
			            'kategori_id' => $request['co_id'],
			            'inisial' => $request['inisial'],
			        ]);
	    		}
    		}else{
    			if($request['kategori'] == 1){
    				$user = User::create([
	    				'nip' => $request['nip'],
	    				'name' => $request['name'],
			            'email' => $request['email'],
			            'phone' => $request['phone'],
			            'password' => Hash::make($request['password']),
			            'tipe' => $request['tipe'],
			            'kategori' => $request['kategori'],
			            'kategori_id' => $request['bu_id'],
			        ]);
    			}else{
    				$user = User::create([
	    				'nip' => $request['nip'],
	    				'name' => $request['name'],
			            'email' => $request['email'],
			            'phone' => $request['phone'],
			            'password' => Hash::make($request['password']),
			            'tipe' => $request['tipe'],
			            'kategori' => $request['kategori'],
			            'kategori_id' => $request['co_id'],
			        ]);
    			}
    		}
    	}else{
    		if($request['kategori'] == 1){
    			$user = User::create([
					'nip' => $request['nip'],
					'name' => $request['name'],
		            'email' => $request['email'],
		            'phone' => $request['phone'],
		            'password' => Hash::make($request['password']),
		            'tipe' => $request['tipe'],
		            'kategori' => $request['kategori'],
		            'kategori_id' => $request['bu_id'],
		        ]);
			}else{
				$user = User::create([
					'nip' => $request['nip'],
					'name' => $request['name'],
		            'email' => $request['email'],
		            'phone' => $request['phone'],
		            'password' => Hash::make($request['password']),
		            'tipe' => $request['tipe'],
		            'kategori' => $request['kategori'],
		            'kategori_id' => $request['co_id'],
		        ]);
			}
    	}

    	$users = auth()->user();
    	$log = new SessionLog;
	    $log->modul = 'Manajemen Pengguna';
	    $log->sub = 'User';
	    $log->aktivitas = 'Membuat User baru dengan nama '.$request['name'];
	    $log->user_id = $users->id;
	    $log->save();

	    if($request['tipe'] == 1){
	        $user->roles()->sync($request->roles);
	    }else{
	    	$user->roles()->sync($request->role);
	    }

        return $user;
    }

    public function show(User $user)
    {
        return $user->toJson();
    }

    public function edit(User $user)
    {
        return $this->render('settings.user.edit', ['record' => $user]);
    }

    public function update(UserRequest $request, User $user)
    {
    	if($request['tipe']== 1){
    		if($request['roles'] == 10){
    			if($request['kategori'] == 1){
    				$attrs = [
	    				'nip' => $request['nip'],
	    				'name' => $request['name'],
			            'email' => $request['email'],
			            'phone' => $request['phone'],
		                'fungsi' => $request['fungsi'],
			            'tipe' => $request['tipe'],
			            'kategori' => $request['kategori'],
			            'kategori_id' => $request['bu_id'],
			            'inisial' => $request['inisial'],
			        ];
	    		}else{
	    			$attrs = [
		    			'nip' => $request['nip'],
	    				'name' => $request['name'],
			            'email' => $request['email'],
			            'phone' => $request['phone'],
		                'fungsi' => $request['fungsi'],
			            'tipe' => $request['tipe'],
			            'kategori' => $request['kategori'],
			            'kategori_id' => $request['co_id'],
			            'inisial' => $request['inisial'],
			        ];
	    		}
    		}else{
    			if($request['kategori'] == 1){
    				$attrs = [
	    				'nip' => $request['nip'],
	    				'name' => $request['name'],
			            'email' => $request['email'],
			            'phone' => $request['phone'],
			            'tipe' => $request['tipe'],
			            'kategori' => $request['kategori'],
			            'kategori_id' => $request['bu_id'],
			        ];
    			}else{
    				$attrs = [
	    				'nip' => $request['nip'],
	    				'name' => $request['name'],
			            'email' => $request['email'],
			            'phone' => $request['phone'],
			            'tipe' => $request['tipe'],
			            'kategori' => $request['kategori'],
			            'kategori_id' => $request['co_id'],
			        ];
    			}
    		}
    	}else{
    		if($request['kategori'] == 1){
    			$attrs = [
					'nip' => $request['nip'],
					'name' => $request['name'],
		            'email' => $request['email'],
		            'phone' => $request['phone'],
		            'tipe' => $request['tipe'],
		            'kategori' => $request['kategori'],
		            'kategori_id' => $request['bu_id'],
		        ];
			}else{
				$attrs = [
					'nip' => $request['nip'],
					'name' => $request['name'],
		            'email' => $request['email'],
		            'phone' => $request['phone'],
		            'tipe' => $request['tipe'],
		            'kategori' => $request['kategori'],
		            'kategori_id' => $request['co_id'],
		        ];
			}
    	}

        $users = auth()->user();
    	$log = new SessionLog;
	    $log->modul = 'Manajemen Pengguna';
	    $log->sub = 'User';
	    $log->aktivitas = 'Mengubah '.$request['name'];
	    $log->user_id = $users->id;
	    $log->save();

        if ($pass = $request->password) {
            $attrs['password'] = Hash::make($pass);
        }

        $user->update($attrs);
        if($request['tipe'] == 1){
	        $user->roles()->sync($request->roles);
        }else{
        	$user->roles()->sync($request->role);
        }

        return $user;
    }

    public function destroy(User $user)
    {
        if($user){
    		$project = ProjectDetailPic::where('pic', $user->id)->get()->count();
    		$ap = AnakPerusahaanDetailPic::where('pic', $user->id)->get()->count();
    		$bu = BUDetailPic::where('pic', $user->id)->get()->count();
    		$co = CODetailPic::where('pic', $user->id)->get()->count();
    		$ve = VendorDetailPic::where('pic', $user->id)->get()->count();
    		if($project > 0 || $ap > 0 || $bu > 0 || $co > 0 || $ve > 0){
	        	return response([
                	'status' => 500,
            	],500);
        	}else{
        		$users = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Manajemen Pengguna';
			    $log->sub = 'User';
			    $log->aktivitas = 'Menghapus User '.$user->name;
			    $log->user_id = $users->id;
			    $log->save();

        		$user->delete();
	        	return response([
	            	'status' => true,
		        ],200);
        	}
    	}else{
    		return response([
                'status' => 500,
            ],500);
    	}
    }
}
