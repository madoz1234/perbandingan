<?php
namespace App\Http\Middleware;

use Closure;
use Menu;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('sideMenu', function ($menu) {
            $menu->add('TOPSIS', 'topsis')
                  ->data('icon', 'fa fa-money');
	             $menu->tOPSIS->add('Nilai Alternatif', 'topsis/nilai')
                 	 ->data('perms', 'topsis-nilai')
	            	 ->active('topsis/nilai/*');
	           	 $menu->tOPSIS->add('Hasil', 'topsis/hasil')
                 	 ->data('perms', 'topsis-hasil')
	            	 ->active('topsis/hasil/*');

	        $menu->add('AHP', 'ahp')
                  ->data('icon', 'fa fa-money');
	             $menu->aHP->add('Ratio Kriteria', 'ahp/kriteria')
                 	 ->data('perms', 'ahp-kriteria')
	            	 ->active('ahp/kriteria/*');
	           	 $menu->aHP->add('Ratio Alternatif', 'ahp/alternatif')
                     ->data('perms', 'ahp-alternatif')
                     ->active('ahp/alternatif/*');

            $menu->add('Data Master', 'master')
                  ->data('icon', 'fa fa-book');
	             $menu->dataMaster->add('Kriteria', 'master/kriteria')
                 	 ->data('perms', 'master-kriteria')
	            	 ->active('master/kriteria/*');
	             $menu->dataMaster->add('Alternatif', 'master/alternatif')
                 	 ->data('perms', 'master-alternatif')
	            	 ->active('master/alternatif/*');
        });
        return $next($request);
    }
}
