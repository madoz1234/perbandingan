<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\Produk;

class Harga extends Model
{
    /* default */
    protected $table 		= 'ref_harga';
    protected $fillable 	= ['produk_id','harga','stok'];

    /* data ke log */
    protected $log_table    = 'log_ref_harga';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function produk(){
        return $this->belongsTo(Produk::class, 'produk_id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
