<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Topsis\NilaiAlternatif;

class Alternatif extends Model
{
    /* default */
    protected $table 		= 'ref_alternatif';
    protected $fillable 	= ['nama'];

    /* data ke log */
    protected $log_table    = 'log_ref_alternatif';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function nilaialternatif(){
        return $this->hasOne(NilaiAlternatif::class, 'alternatif_id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
