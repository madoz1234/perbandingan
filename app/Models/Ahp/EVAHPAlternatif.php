<?php

namespace App\Models\Ahp;

use App\Models\Model;
use App\Models\Master\Alternatif;

class EVAHPAlternatif extends Model
{
    /* default */
    protected $table 		= 'trans_ev_alternatif';
    protected $fillable 	= ['alternatifid1','alternatifid2','ev','nilai'];

    /* data ke log */
    protected $log_table    = 'log_trans_ev_alternatif';
    protected $log_table_fk = 'ref_id';
    /* relation */
    
    public function alternatif1(){
        return $this->belongsTo(Alternatif::class, 'alternatifid1' , 'id');
    }

    public function alternatif2(){
        return $this->belongsTo(Alternatif::class, 'alternatifid2' , 'id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
