<?php

namespace App\Models\Topsis;

use App\Models\Model;
use App\Models\Master\Alternatif;
use App\Models\Topsis\NilaiAlternatifDetail;

class NilaiAlternatif extends Model
{
    /* default */
    protected $table 		= 'trans_nilai_alternatif_topsis';
    protected $fillable 	= ['alternatif_id','nilai'];

    /* data ke log */
    protected $log_table    = 'log_trans_nilai_alternatif_topsis';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function detail(){
        return $this->hasMany(NilaiAlternatifDetail::class, 'nilai_id' , 'id');
    }

    public function alternatif(){
        return $this->belongsTo(Alternatif::class, 'alternatif_id' , 'id');
    }

    public function saveDetail($detail)
    {   
        if($detail){
            foreach ($detail as $key => $value) {
                $nilaidetail = new NilaiAlternatifDetail;
                $nilaidetail->kriteria_id = $key;
                $nilaidetail->nilai = $value['nilai'];
                $this->detail()->save($nilaidetail);
            }
        }
    }

    public function updateDetail($detail)
    {   
        if($detail){
            $cari = NilaiAlternatifDetail::where('nilai_id', $this->id)->delete();
            foreach ($detail as $key => $value) {
                $news = new NilaiAlternatifDetail;
                $news->kriteria_id = $key;
                $news->nilai = $value['nilai'];
                $this->detail()->save($news);
            }
        }
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
