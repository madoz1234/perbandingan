<?php
if (!function_exists('DisplayStatusRole')){
  function DisplayStatusRole($string) {
    $return = '-';
    switch ($string)
    {
        case 'User Proyek': $return = 'Proyek';
        break;
        case 'User Head Office': $return = 'Head Office';
        break;
        case 'User Divisi': $return = 'Wilayah';
        break;
    }

    return $return;
  }
}



if (!function_exists('wrapText')){
  function wrapText($string) {
    return wordwrap($string, 50, '<br>\n');
  }
}

if (!function_exists('cekCR')){
  function cekCR($string) {

    $a = intval($string);
    if($a == 1){
        return 0;
    }elseif($a == 2){
        return 0;
    }elseif($a == 3){
        return 0.58;
    }elseif($a == 4){
        return 0.9;
    }elseif($a == 5){
        return 1.12;
    }elseif($a == 6){
        return 1.24;
    }elseif($a == 7){
        return 1.32;
    }elseif($a == 8){
        return 1.41;
    }elseif($a == 9){
        return 1.45;
    }elseif($a == 10){
        return 1.49;
    }else{
        return 0;
    }
  }
}

if (!function_exists('validasi_upload_file')){
  function validasi_upload_file($data) {
    $jumlah = 0;
    for($i=0;$i<=12;$i++){
        if($i == 0){
            if(!empty($data[0])){
                 $cek = App\Models\Audit\Rencana\RencanaAudit::where('nama', $data[0])->get();
                 if($cek->count() > 0){
                    $jumlah +=1;
                 }
            }else{
                $jumlah +=1;
            }
        }elseif($i == 1){
            if(!is_numeric($data[1])){
                $jumlah +=1;
            }
        }elseif($i == 2){
            if(!is_numeric($data[2])){
                $jumlah +=1;
            }
        }elseif($i == 3){
            if(!is_numeric($data[3])){
                $jumlah +=1;
            }
        }elseif($i == 4){
            if(!is_numeric($data[4])){
                $jumlah +=1;
            }
        }elseif($i == 5){
            if(is_numeric($data[5])){
                 if($data[5] >= 0 && $data[5] < 5){
                 }else{
                    $jumlah +=1;
                 }
            }else{
                $jumlah +=1;
            }
        }elseif($i == 6){
            if(!is_numeric($data[6])){
                $jumlah +=1;
            }
        }elseif($i == 7){
            if(!is_numeric($data[7])){
                $jumlah +=1;
            }
        }elseif($i == 8){
            if(!is_numeric($data[8])){
                $jumlah +=1;
            }
        }elseif($i == 9){
            if(!empty($data[9])){
                 if($data[9] == 0 OR $data[9] > 0 OR $data[9] < 5){
                 }else{
                    $jumlah +=1;
                 }
            }else{
                $jumlah +=1;
            }
        }elseif($i == 10){
            if(is_numeric($data[10])){
                 if($data[10] < 0){
                    $jumlah +=1;
                 }
            }else{
                $jumlah +=1;
            }
        }elseif($i == 11){
            if(is_numeric($data[11])){
                 if($data[11] >= 0 || $data[11] < 4){
                 }else{
                    $jumlah +=1;
                 }
            }else{
                $jumlah +=1;
            }
        }elseif($i == 12){
            if(is_numeric($data[12])){
                 if($data[12] >= 0){
                 }else{
                    $jumlah +=1;
                 }
            }else{
                $jumlah +=1;
            }
        }else{
        }
    }
    return $jumlah;
  }
}

if (!function_exists('formatEnMonth')){
  function formatEnMonth($bulan) {
    $blnInt = 0;
    switch ($bulan)
    {
        case 'Januari': $blnInt = 'January';
        break;
        case 'Februari': $blnInt = 'February';
        break;
        case 'Maret': $blnInt = 'March';
        break;
        case 'April': $blnInt = 'April';
        break;
        case 'Mei': $blnInt = 'May';
        break;
        case 'Juni': $blnInt = 'June';
        break;
        case 'Juli': $blnInt = 'July';
        break;
        case 'Agustus': $blnInt = 'August';
        break;
        case 'September': $blnInt = 'September';
        break;
        case 'Oktober': $blnInt = 'October';
        break;
        case 'November': $blnInt = 'November';
        break;
        case 'Desember': $blnInt = 'December';
        break;
    }

    return $blnInt;
  }
}

if (!function_exists('romawi')){
  function romawi($number) {
    $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
    $returnValue = '';
    while ($number > 0) {
        foreach ($map as $roman => $int) {
            if($number >= $int) {
                $number -= $int;
                $returnValue .= $roman;
                break;
            }
        }
    }
    return $returnValue;
  }
}

if (!function_exists('formatNumMonth')){
  function formatNumMonth($bulan) {
    $blnInt = 0;
    switch ($bulan)
    {
        case 'Januari': $blnInt = 1;
        break;
        case 'Februari': $blnInt = 2;
        break;
        case 'Maret': $blnInt = 3;
        break;
        case 'April': $blnInt = 4;
        break;
        case 'Mei': $blnInt = 5;
        break;
        case 'Juni': $blnInt = 6;
        break;
        case 'Juli': $blnInt = 7;
        break;
        case 'Agustus': $blnInt = 8;
        break;
        case 'September': $blnInt = 9;
        break;
        case 'Oktober': $blnInt = 10;
        break;
        case 'November': $blnInt = 11;
        break;
        case 'Desember': $blnInt = 12;
        break;
    }

    return $blnInt;
  }
}

if (!function_exists('formatBarcode')){
  function formatBarcode($no_order, $jenis, $no_seri, $index) {
        return $no_order."-".$jenis."-".$no_seri."-".$index;
  }
}

if (!function_exists('formatStringMonth')){
  function formatStringMonth($bulan) {
    $blnInt = 0;
    switch ($bulan)
    {
        case '01': $blnInt = 'Januari';
        break;
        case '02': $blnInt = 'Februari';
        break;
        case '03': $blnInt = 'Maret';
        break;
        case '04': $blnInt = 'April';
        break;
        case '05': $blnInt = 'Mei';
        break;
        case '06': $blnInt = 'Juni';
        break;
        case '07': $blnInt = 'Juli';
        break;
        case '08': $blnInt = 'Agustus';
        break;
        case '09': $blnInt = 'September';
        break;
        case '10': $blnInt = 'Oktober';
        break;
        case '11': $blnInt = 'November';
        break;
        case '12': $blnInt = 'Desember';
        break;
    }

    return $blnInt;
  }
}

if (!function_exists('DiffMnY')){
  function DiffMnY($bulan, $tahun) {
    $start = Carbon\Carbon::parse('first day of '.formatEnMonth($bulan).' '.$tahun);
    $end = Carbon\Carbon::parse('last day of '.formatEnMonth($bulan).' '.$tahun);

    return $start->diffInDays($end);
  }
}

if (!function_exists('DateToSql')) {
    function DateToSql($date) {
        if($date != NULL)
        {
            $pecah = explode(" ", $date);
            $tglStr = str_replace(",", "", $pecah[1]);
            if(strlen($tglStr) == 1)
            {
                $tglStr = "0".$tglStr;
            }
            $thnStr = $pecah[2];
            $blnStr = "";
            switch ($pecah[0])
            {
                case 'Januari': $blnStr = '01';
                break;
                case 'Februari': $blnStr = '02';
                break;
                case 'Maret': $blnStr = '03';
                break;
                case 'April': $blnStr = '04';
                break;
                case 'Mei': $blnStr = '05';
                break;
                case 'Juni': $blnStr = '06';
                break;
                case 'Juli': $blnStr = '07';
                break;
                case 'Agustus': $blnStr = '08';
                break;
                case 'September': $blnStr = '09';
                break;
                case 'Oktober': $blnStr = '10';
                break;
                case 'November': $blnStr = '11';
                break;
                case 'Desember': $blnStr = '12';
                break;
            }
            return $thnStr."-".$blnStr."-".$tglStr;
        }else{
            return NULL;
        }
    }
}

if (!function_exists('DayOf')) {
    function DayOf($date){
		$cet = \Carbon\Carbon::parse($date)->format('Y-m-d');
		$cet = \Carbon\Carbon::parse($cet);
      switch ($cet->format('l'))
      {
          case 'Sunday': return 'Minggu';
          break;
          case 'Monday': return 'Senin';
          break;
          case 'Tuesday': return 'Selasa';
          break;
          case 'Wednesday': return 'Rabu';
          break;
          case 'Thursday': return 'Kamis';
          break;
          case 'Friday': return 'Jumat';
          break;
          case 'Saturday': return 'Sabtu';
          break;
      }
    }
}

if (!function_exists('imageShow')) {
    function imageShow($file) {
      if($file->count() > 0)
      {
          $str = explode(".",$file->first()->url);

          if(strtolower($str[1]) == 'jpeg' || strtolower($str[1]) == 'png' || strtolower($str[1]) == 'jpg')
          {
              return url('storage/'.$file->first()->url);
          }

          return asset('img/archive.png');
      }
      return asset('img/no-images.png');
    }
}

if (!function_exists('singleImageShow')) {
    function singleImageShow($file) {
      $str = explode(".",$file->url);

      if(strtolower($str[1]) == 'jpeg' || strtolower($str[1]) == 'png' || strtolower($str[1]) == 'jpg')
      {
          return url('storage/'.$file->url);
      }

      return asset('img/archive.png');
    }
}

if (!function_exists('DateToString')) {
    function DateToString($date) {
        if(!$date)
        {
            return '-';
        }
        // $date = new DateTime($date);
        $date = \Carbon\Carbon::parse($date)->format('Y-m-d');
        // $tgl = $date->format('Y-m-d');
        $pecah = explode("-", $date);
        $thnStr = $pecah[0];
        $tglStr = $pecah[2]." ";
        $blnStr = "";
        switch ($pecah[1])
        {
            case '01': $blnStr = 'Januari';
            break;
            case '02': $blnStr = 'Februari';
            break;
            case '03': $blnStr = 'Maret';
            break;
            case '04': $blnStr = 'April';
            break;
            case '05': $blnStr = 'Mei';
            break;
            case '06': $blnStr = 'Juni';
            break;
            case '07': $blnStr = 'Juli';
            break;
            case '08': $blnStr = 'Agustus';
            break;
            case '09': $blnStr = 'September';
            break;
            case '10': $blnStr = 'Oktober';
            break;
            case '11': $blnStr = 'November';
            break;
            case '12': $blnStr = 'Desember';
            break;
        }
        return $tglStr." ".$blnStr." ".$thnStr;
    }
}

if (!function_exists('DateToStrings')) {
    function DateToStrings($date) {
        if(!$date)
        {
            return '-';
        }
        $pecah = explode("/", $date);
        $thnStr = $pecah[2];
        $tglStr = $pecah[0]." ";
        $blnStr = "";
        switch ($pecah[1])
        {
            case '01': $blnStr = 'Januari';
            break;
            case '02': $blnStr = 'Februari';
            break;
            case '03': $blnStr = 'Maret';
            break;
            case '04': $blnStr = 'April';
            break;
            case '05': $blnStr = 'Mei';
            break;
            case '06': $blnStr = 'Juni';
            break;
            case '07': $blnStr = 'Juli';
            break;
            case '08': $blnStr = 'Agustus';
            break;
            case '09': $blnStr = 'September';
            break;
            case '10': $blnStr = 'Oktober';
            break;
            case '11': $blnStr = 'November';
            break;
            case '12': $blnStr = 'Desember';
            break;
        }
        return $tglStr." ".$blnStr." ".$thnStr;
    }
}

if (!function_exists('BulanWithYear')) {
    function BulanWithYear($date) {
        if(!$date)
        {
            return '-';
        }
        $pecah = explode("-", $date);
        $tahun = $pecah[1];
        switch ($pecah[0])
        {
            case '01': $blnStr = 'Januari';
            break;
            case '02': $blnStr = 'Februari';
            break;
            case '03': $blnStr = 'Maret';
            break;
            case '04': $blnStr = 'April';
            break;
            case '05': $blnStr = 'Mei';
            break;
            case '06': $blnStr = 'Juni';
            break;
            case '07': $blnStr = 'Juli';
            break;
            case '08': $blnStr = 'Agustus';
            break;
            case '09': $blnStr = 'September';
            break;
            case '10': $blnStr = 'Oktober';
            break;
            case '11': $blnStr = 'November';
            break;
            case '12': $blnStr = 'Desember';
            break;
        }
        return $blnStr." ".$tahun;
    }
}

if (!function_exists('BulanToString')) {
    function BulanToString($date) {
        if(!$date)
        {
            return '-';
        }
        $pecah = explode("-", $date);
        $thnStr = $pecah[1];
        $blnStr = "";
        switch ($pecah[0])
        {
            case '01': $blnStr = 'Januari';
            break;
            case '02': $blnStr = 'Februari';
            break;
            case '03': $blnStr = 'Maret';
            break;
            case '04': $blnStr = 'April';
            break;
            case '05': $blnStr = 'Mei';
            break;
            case '06': $blnStr = 'Juni';
            break;
            case '07': $blnStr = 'Juli';
            break;
            case '08': $blnStr = 'Agustus';
            break;
            case '09': $blnStr = 'September';
            break;
            case '10': $blnStr = 'Oktober';
            break;
            case '11': $blnStr = 'November';
            break;
            case '12': $blnStr = 'Desember';
            break;
        }
        return $blnStr." ".$thnStr;
    }
}

if (!function_exists('TahunToString')) {
    function TahunToString($date) {
        if(!$date)
        {
            return '-';
        }
        $pecah = explode("-", $date);
        $thnStr = $pecah[2];
        return $thnStr;
    }
}

if (!function_exists('DateToStringWday')) {
    function DateToStringWday($date) {
        if(!$date)
        {
            return '-';
        }
        $tgl = \Carbon\Carbon::parse($date)->format('Y-m-d');
        $pecah = explode("-", $tgl);
        $thnStr = $pecah[0];
        $tglStr = $pecah[2]."";
        $blnStr = "";
        switch ($pecah[1])
        {
            case '01': $blnStr = 'Januari';
            break;
            case '02': $blnStr = 'Februari';
            break;
            case '03': $blnStr = 'Maret';
            break;
            case '04': $blnStr = 'April';
            break;
            case '05': $blnStr = 'Mei';
            break;
            case '06': $blnStr = 'Juni';
            break;
            case '07': $blnStr = 'Juli';
            break;
            case '08': $blnStr = 'Agustus';
            break;
            case '09': $blnStr = 'September';
            break;
            case '10': $blnStr = 'Oktober';
            break;
            case '11': $blnStr = 'November';
            break;
            case '12': $blnStr = 'Desember';
            break;
        }
        return $tglStr." ".$blnStr." ".$thnStr;
    }
}

if (!function_exists('statusReview')) {
    function statusReview($status, $url = null) {
        switch ($status)
        {
            case 0: return '<a class="ui tag label" href="'.$url.'">Belum Dibaca</a>';
            break;
            case 1: return '<a class="ui teal tag label" href="'.$url.'">Sudah Dibaca</a>';
            break;
        }
    }
}

if (!function_exists('statusLabel')) {
    function statusLabel($status, $url) {
        switch ($status)
        {
            case 0: return '<a href="'.url($url).'" class="ui orange ribbon label">Belum Dibaca</a>';
            break;
            case 1: return '<a href="'.url($url).'" class="ui teal ribbon label">Sudah Dibaca</a>';
            break;
        }
    }
}

if (!function_exists('statusTindakan')) {
    function statusTindakan($status) {
        switch ($status)
        {
            case 0: return '<a href="javascript:void(0)" class="ui orange tag label">Belum Selesai</a>';
            break;
            case 1: return '<a href="javascript:void(0)" class="ui teal tag label">Selesai</a>';
            break;
        }
    }
}

if (!function_exists('imageItem')) {
  function imageItem($picture) {
      if($picture)
      {
        return '<img src="'.url('storage/'.$picture).'" style="height:8rem">';
      }
      return '<img src="'.asset('img/no-images.png').'" style="height:8rem">';
  }
}

if (!function_exists('readMoreText')) {
    function readMoreText($value, $maxLength = 150)
    {
        $return = textarea($value);
        if (strlen($value) > $maxLength) {
            $return = substr(textarea($value), 0, $maxLength);
            $readmore = substr(textarea($value), $maxLength);

            $return .= '<a href="javascript: void(0)" class="read-more text-info" onclick="$(this).parent().find(\'.read-more-cage\').show(); $(this).hide()">&nbsp;&nbsp;Selengkapnya...</a>';

            $readless = '<a href="javascript: void(0)" class="read-less text-info" onclick="$(this).parent().parent().find(\'.read-more\').show(); $(this).parent().hide()">&nbsp;&nbsp;Kecilkan...</a>';

            $return = "<span>{$return}<span style='display: none' class='read-more-cage'>{$readmore} {$readless}</span></span>";
        }
        return $return;
    }
}

if (!function_exists('stringTakenAt')) {
    function stringTakenAt($taken, $waktu, $tanggal)
    {
        $pecah = explode(" ", $waktu);
        $p = explode(":", $pecah[0]);
        $hours = (int)$p[0];
        $minutes = (int)$p[1];
        if($pecah[1] == 'PM')
        {
            $hours = $hours + 12;
        }
        $ex = explode(" ", $tanggal);

        $fullDate = Carbon\Carbon::parse($ex[0]." ".$hours.":".$minutes.":00");
        $diff = $fullDate->diffInHours($taken);
        if($diff > 2)
        {
            return '<span style="font-size:10px;color:red;"><i>Data diambil diatas 2 jam pada : '.$taken->format('F d, Y (H:i:s)').'</i></span>';
        }
        return '<span style="font-size:10px;color:green;"><i>Data diambil dibawah 2 jam pada : '.$taken->format('F d, Y (H:i:s)').'</i></span>';
    }
}

if (!function_exists('textarea')) {
    function textarea($text)
    {
        $new = '';

        $new = str_replace("\n", "<br>", $text);

        return $new;
    }
}

if (!function_exists('column_letter')) {
    function column_letter($c) {
        $c = intval($c);

        if ($c <= 0)
            return '';

        $letter = '';
        while($c != 0){
           $p = ($c - 1) % 26;
           $c = intval(($c - $p) / 26);
           $letter = chr(65 + $p) . $letter;
        }

        return strtolower($letter.'.');
    }
}

if (!function_exists('column_letters')) {
    function column_letters($c) {
        $c = intval($c);

        if ($c <= 0)
            return '';

        $letter = '';
        while($c != 0){
           $p = ($c - 1) % 26;
           $c = intval(($c - $p) / 26);
           $letter = chr(65 + $p) . $letter;
        }

        return $letter.$letter;
    }
}

if (!function_exists('numDay')) {
    function numDay($day) {
        if($day != NULL)
        {
            $blnStr = "";
            switch ($day)
            {
                case '0': $blnStr = 'Senin';
                break;
                case '1': $blnStr = 'Selasa';
                break;
                case '2': $blnStr = 'Rabu';
                break;
                case '3': $blnStr = 'Kamis';
                break;
                case '4': $blnStr = 'Jumat';
                break;
                case '5': $blnStr = 'Sabtu';
                break;
                case '6': $blnStr = 'Minggu';
                break;
            }
            return $blnStr;
        }else{
            return '';
        }
    }
}

if (!function_exists('ipAddress')) {
    function ipAddress(){
        switch(true){
      case (!empty($_SERVER['HTTP_X_REAL_IP'])) : return $_SERVER['HTTP_X_REAL_IP'];
      case (!empty($_SERVER['HTTP_CLIENT_IP'])) : return $_SERVER['HTTP_CLIENT_IP'];
      case (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) : return $_SERVER['HTTP_X_FORWARDED_FOR'];
      default : return $_SERVER['REMOTE_ADDR'];
    }
    }
}

if (!function_exists('slugify')) {
    function slugify($text){
          // replace non letter or digits by -
          $text = preg_replace('~[^\pL\d]+~u', '-', $text);

          // transliterate
          $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

          // remove unwanted characters
          $text = preg_replace('~[^-\w]+~', '', $text);

          // trim
          $text = trim($text, '-');

          // remove duplicate -
          $text = preg_replace('~-+~', '-', $text);

          // lowercase
          $text = strtolower($text);

          if (empty($text)) {
            return 'n-a';
          }

          return $text;
        }
}

if (!function_exists('tipeCostumers')) {
    function tipeCostumers($data)  
    {  
        if(!is_null($data))
        {
            switch ($data)
            {
                case 0: $costumer = 'PLN';
                break;
                case 1: $costumer = 'NON-PLN';
                break;
                case 2: $costumer = 'A-PLN';
                break;
            }
            return $costumer;
        }else{
            return '';
        }  
    }  
}

if (!function_exists('DateToStringYear')) {
    function DateToStringYear($date) {
        if(!$date)
        {
            return '-';
        }
        $date = \Carbon\Carbon::parse($date)->format('Y-m-d');
        $pecah = explode("-", $date);
        $thnStr = $pecah[0];
        $tglStr = $pecah[2];
        $blnStr = "";
        switch ($pecah[1])
        {
            case '01': $blnStr = 'Januari';
            break;
            case '02': $blnStr = 'Februari';
            break;
            case '03': $blnStr = 'Maret';
            break;
            case '04': $blnStr = 'April';
            break;
            case '05': $blnStr = 'Mei';
            break;
            case '06': $blnStr = 'Juni';
            break;
            case '07': $blnStr = 'Juli';
            break;
            case '08': $blnStr = 'Agustus';
            break;
            case '09': $blnStr = 'September';
            break;
            case '10': $blnStr = 'Oktober';
            break;
            case '11': $blnStr = 'November';
            break;
            case '12': $blnStr = 'Desember';
            break;
        }
        return $tglStr." ".$blnStr." ".$thnStr;
    }
}

if (!function_exists('MonthWithYear')) {
    function MonthWithYear($date) {
        if(!$date)
        {
            return '-';
        }
        $pecah = explode("-", $date);
        $thnStr = $pecah[1];
        $blnStr = "";
        switch ($pecah[0])
        {
            case '01': $blnStr = 'Januari';
            break;
            case '02': $blnStr = 'Februari';
            break;
            case '03': $blnStr = 'Maret';
            break;
            case '04': $blnStr = 'April';
            break;
            case '05': $blnStr = 'Mei';
            break;
            case '06': $blnStr = 'Juni';
            break;
            case '07': $blnStr = 'Juli';
            break;
            case '08': $blnStr = 'Agustus';
            break;
            case '09': $blnStr = 'September';
            break;
            case '10': $blnStr = 'Oktober';
            break;
            case '11': $blnStr = 'November';
            break;
            case '12': $blnStr = 'Desember';
            break;
        }
        return $blnStr." ".$thnStr;
    }
}

if(!function_exists('FormatNumber')){
    function FormatNumber($int){
        return number_format($int, 0, ',', '.');
    }
}

if (!function_exists('decimal_for_save')) {
    function decimal_for_save($text)
    {
        $text_edit = str_replace('.', '', $text);
        $newtext = str_replace(',', '.', $text_edit);

        return $newtext;
    }
}

if (!function_exists('removeZeroDigitsFromDecimal')) {
    function removeZeroDigitsFromDecimal($number, $decimal_sep = ',')
    {
        $explode_num = explode($decimal_sep, $number);
        if (is_array($explode_num) && isset($explode_num[count($explode_num)-1]) && intval($explode_num[count($explode_num)-1]) === 0) {
            unset($explode_num[count($explode_num)-1]);
            $number = implode($decimal_sep, $explode_num);
        }
        unset($explode_num);
        return (string) $number;
    }
}

if (!function_exists('getNamaObjectBU')) {
    function getNamaObjectBU()
    {

        $bu = \App\Models\Master\BU::all();
        return $bu;
        // $data = \App\Models\Act\ActPengujian::whereIn('parent_id', $detail)->orderBy('created_at','desc')->get();
    }
}

if (!function_exists('getNamaObjectCO')) {
    function getNamaObjectCO()
    {
        
        $co = \App\Models\Master\CO::all();
        return $co;
        // $data = \App\Models\Act\ActPengujian::whereIn('parent_id', $detail)->orderBy('created_at','desc')->get();
    }
}

if (!function_exists('getNamaObjectAP')) {
    function getNamaObjectAP()
    {
        
        $ap = \App\Models\Master\AnakPerusahaan::all();
        return $ap;
        // $data = \App\Models\Act\ActPengujian::whereIn('parent_id', $detail)->orderBy('created_at','desc')->get();
    }
}

if (!function_exists('getNamaObjectVendor')) {
    function getNamaObjectVendor()
    {
        
        $vendor = \App\Models\Master\Vendor::all();
        return $vendor;
        // $data = \App\Models\Act\ActPengujian::whereIn('parent_id', $detail)->orderBy('created_at','desc')->get();
    }
}

if (!function_exists('getParentBu')) {
    function getParentBu($id)
    {
        
        $project = \App\Models\Master\Project::find($id);
            if($project){
                $objek = $project->bu_id;
            }else{
                $objek = '';
            }
        return $objek;
    }
}

if (!function_exists('getParentBuNama')) {
    function getParentBuNama($id)
    {
        
        $project = \App\Models\Master\Project::find($id);
            if($project){
                $objek = $project->bu->nama;
            }else{
                $objek = '';
            }
        return $objek;
    }
}


if (!function_exists('checkParent')) {
    function checkParent($id, $idx)
    {
        $cek1 = null;
        if($id){
            $cek1 = \App\Models\KegiatanAudit\Rencana\RencanaAudit::where('parent_id', $id)
                                                              ->get()->count();
            if($cek1 > 2){
                $cek2 = \App\Models\KegiatanAudit\Rencana\RencanaAudit::where('parent_id', $id)->orderBy('created_at', 'desc')->skip(1)->take(1)->first();
                if($cek2->id == $idx){
                	$cek2 = \App\Models\KegiatanAudit\Rencana\RencanaAudit::where('parent_id', $id)
                                                              ->get()->last();
                }else{
                	$cek2 = $cek2;
                }

                $return = $cek2->status;
            }else{
                $cek2 = \App\Models\KegiatanAudit\Rencana\RencanaAudit::where('parent_id', $id)
                                                              ->get()->last();
                $return = $cek2->status;
            }
        }

        return $return;
    }
}

if (!function_exists('checkGroup')) {
    function checkGroup($id)
    {
        $cek1 = null;
        if($id){
            $cek1 = \App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit::where('group', $id)->get();
        }
        return $cek1;
    }
}

if (!function_exists('object_audit')) {
    function object_audit($kategori, $id)
    {
        $objek='';
        if($kategori == 0){
            $bu = \App\Models\Master\BU::find($id);
            if($bu){
                $objek =$bu->nama;
            }else{
                $objek ='';
            }
        }elseif($kategori == 1){
            $cu = \App\Models\Master\CO::find($id);
            if($cu){
                $objek =$cu->nama;
            }else{
                $objek ='';
            }
        }elseif($kategori == 2){
            $project = \App\Models\Master\Project::find($id);
            if($project){
                $objek =$project->nama;
            }else{
                $objek ='';
            }
        }else{
            $ap = \App\Models\Master\AnakPerusahaan::find($id);
            if($ap){
                $objek =$ap->nama;
            }else{
                $objek ='';
            }
        }
        return $objek;
        // $data = \App\Models\Act\ActPengujian::whereIn('parent_id', $detail)->orderBy('created_at','desc')->get();
    }
}

if (!function_exists('findbu')) {
    function findbu($id)
    {
        $objek='';
        $project = \App\Models\Master\Project::find($id);
        return $project->bu->id;
        // $data = \App\Models\Act\ActPengujian::whereIn('parent_id', $detail)->orderBy('created_at','desc')->get();
    }
}

if (!function_exists('findbus')) {
    function findbus($id)
    {
        $objek='';
        $project = \App\Models\Master\Project::find($id);
        return $project->bu->nama;
        // $data = \App\Models\Act\ActPengujian::whereIn('parent_id', $detail)->orderBy('created_at','desc')->get();
    }
}

if (!function_exists('checkuser')) {
    function checkuser($id)
    {
    	$project = \App\Models\Master\ProjectDetailPic::where('pic', $id)->get();
		$ap = \App\Models\Master\AnakPerusahaanDetailPic::where('pic', $id)->get();
		$bu = \App\Models\Master\BUDetailPic::where('pic', $id)->get();
		$co = \App\Models\Master\CODetailPic::where('pic', $id)->get();
		$ve = \App\Models\Master\VendorDetailPic::where('pic', $id)->get();
		$data = array();
		if(count($bu) > 0){
			$data[]= 0;
			$data[]= $bu->pluck('bu_id')->toArray();
			return $data;
		}elseif(count($co) > 0){
			$data[]= 1;
			$data[]= $co->pluck('co_id')->toArray();
			return $data;
		}elseif(count($project) > 0){
			$data[]= 2;
			$data[]= $project->pluck('project_id')->toArray();
			return $data;
		}elseif(count($ap) > 0){
			$data[]= 3;
			$data[]= $ap->pluck('ap_id')->toArray();
			return $data;
		}elseif(count($ve) > 0){
			$data[]= 3;
			$data[]= $ve->first()->vendor->id;
			return $data;
		}else{
			$data[]= 0;
			$data[]= 0;
			return $data;
		}
    }
}

if (!function_exists('checkusernama')) {
    function checkusernama($id)
    {
    	$project = \App\Models\Master\ProjectDetailPic::where('pic', $id)->get();
		$ap = \App\Models\Master\AnakPerusahaanDetailPic::where('pic', $id)->get();
		$bu = \App\Models\Master\BUDetailPic::where('pic', $id)->get();
		$co = \App\Models\Master\CODetailPic::where('pic', $id)->get();
		$ve = \App\Models\Master\VendorDetailPic::where('pic', $id)->get();
		$data = array();

		if(count($bu) > 0){
			$data[]= 0;
			$data[]= $bu->first()->bu->id;
			return $data;
		}elseif(count($co) > 0){
			$data[]= 1;
			$data[]= $co->first()->co->id;
			return $data;
		}elseif(count($project) > 0){
			$data[]= 2;
			$data[]= $project->first()->project->id;
			return $data;
		}elseif(count($ap) > 0){
			$data[]= 3;
			$data[]= $ap->first()->ap->id;
			return $data;
		}elseif(count($ve) > 0){
			$data[]= 3;
			$data[]= $ve->first()->vendor->id;
			return $data;
		}else{
			$data[]= 0;
			$data[]= 0;
			return $data;
		}
    }
}

if (!function_exists('getRencana')) {
    function getRencana($id)
    {
    	$objek=array();
    	$objek[0] = \App\Models\KegiatanAudit\Rencana\RencanaAuditDetail::where('id', $id)->first()->tipe_object;
    	$objek[1] = \App\Models\KegiatanAudit\Rencana\RencanaAuditDetail::where('id', $id)->first()->object_id;
    	return $objek;
        // $data = \App\Models\Act\ActPengujian::whereIn('parent_id', $detail)->orderBy('created_at','desc')->get();
    }
}

if (!function_exists('object_audit_value')) {
    function object_audit_value($kategori, $id)
    {
        $objek='';
        if($kategori == 0){
            $bu = \App\Models\Master\BU::find($id);
            if($bu){
                $objek =0;
            }else{
                $objek =1;
            }
        }elseif($kategori == 1){
            $cu = \App\Models\Master\CO::find($id);
            if($cu){
                $objek =0;
            }else{
                $objek =1;
            }
        }elseif($kategori == 2){
            $project = \App\Models\Master\Project::find($id);
            if($project){
                $objek =0;
            }else{
                $objek =1;
            }
        }else{
            $ap = \App\Models\Master\AnakPerusahaan::find($id);
            if($ap){
                $objek =0;
            }else{
                $objek =1;
            }
        }
        return $objek;
        // $data = \App\Models\Act\ActPengujian::whereIn('parent_id', $detail)->orderBy('created_at','desc')->get();
    }
}

if (!function_exists('object_user')) {
    function object_user($kategori, $id)
    {
        $nama='';
        $user_id='';
        $data=[];
        if($kategori == 0){
            $bu = \App\Models\Master\BU::find($id);
            if($bu){
                $data[0]=$bu->detail_pic->pluck('pic')->toArray();
            }else{
                $data[0]=[];
            }
        }elseif($kategori == 1){
            $cu = \App\Models\Master\CO::find($id);
            if($cu){
                $data[0]=$cu->detail_pic->pluck('pic')->toArray();
            }else{
                $data[0]=[];
            }
        }elseif($kategori == 2){
            $project = \App\Models\Master\Project::find($id);
            if($project){
                $data[0]=$project->detail_pic->pluck('pic')->toArray();
            }else{
                $data[0]=[];
            }
        }else{
            $ap = \App\Models\Master\AnakPerusahaan::find($id);
            if($ap){
                $data[0]=$ap->detail_pic->pluck('pic')->toArray();
            }else{
                $data[0]=[];
            }
        }
        return $data;
        // $data = \App\Models\Act\ActPengujian::whereIn('parent_id', $detail)->orderBy('created_at','desc')->get();
    }
}

if (!function_exists('object_users')) {
    function object_users($kategori, $id)
    {
        $nama='';
        $user_id='';
        $data='';
        if($kategori == 0){
            $bu = \App\Models\Master\BU::find($id);
            if($bu){
                $data=$bu->detail_pic->pluck('pic')->first();
            }else{
                $data='';
            }
        }elseif($kategori == 1){
            $cu = \App\Models\Master\CO::find($id);
            if($cu){
                $data=$cu->detail_pic->pluck('pic')->first();
            }else{
                $data='';
            }
        }elseif($kategori == 2){
            $project = \App\Models\Master\Project::find($id);
            if($project){
                $data=$project->detail_pic->pluck('pic')->first();
            }else{
                $data='';
            }
        }else{
            $ap = \App\Models\Master\AnakPerusahaan::find($id);
            if($ap){
                $data=$ap->detail_pic->pluck('pic')->first();
            }else{
                $data='';
            }
        }
        return $data;
        // $data = \App\Models\Act\ActPengujian::whereIn('parent_id', $detail)->orderBy('created_at','desc')->get();
    }
}

if (!function_exists('object_user_nama')) {
    function object_user_nama($kategori, $id)
    {
        $nama    ='';
        $user_id ='';
        $data    =[];
        if($kategori == 0){
            $bu = \App\Models\Master\BU::find($id);
            if($bu){
            	foreach ($bu->detail_pic as $key => $value) {
	                $data[]= $value->user->name;
            	}
            }else{
                $data[]=[];
            }
        }elseif($kategori == 1){
            $cu = \App\Models\Master\CO::find($id);
            if($cu){
            	foreach ($cu->detail_pic as $key => $value) {
	                $data[]= $value->user->name;
            	}
            }else{
                $data[]=[];
            }
        }elseif($kategori == 2){
            $project = \App\Models\Master\Project::find($id);
            if($project){
            	foreach ($project->detail_pic as $key => $vi) {
	                $data[]= $vi->user->name;
            	}
            }else{
                $data[]=[];
            }
        }else{
            $ap = \App\Models\Master\AnakPerusahaan::find($id);
            if($ap){
            	foreach ($ap->detail_pic as $key => $value) {
	                $data[]= $value->user->name;
            	}
            }else{
                $data[]=[];
            }
        }
        return $data;
        // $data = \App\Models\Act\ActPengujian::whereIn('parent_id', $detail)->orderBy('created_at','desc')->get();
    }
}

if (!function_exists('getlokasi')) {
    function getlokasi($id)
    {
        $project = \App\Models\Master\Project::find($id);
        if($project){
            return $project->alamat;
        }else{
            return '';
        }
    }
}

if (!function_exists('getId')) {
    function getId($id)
    {
        $project = \App\Models\Master\Project::find($id);
        if($project){
            return $project->project_id;
        }else{
            return '';
        }
    }
}

if (!function_exists('getNoAb')) {
    function getNoAb($id)
    {
        $project = \App\Models\Master\Project::find($id);
        if($project){
            return $project->project_ab;
        }else{
            return '';
        }
    }
}

if (!function_exists('getAb')) {
    function getAb($kategori, $id)
    {
        $objek='';
        if($kategori == 2){
            $project = \App\Models\Master\Project::find($id);
            if($project){
                $objek =$project->project_ab;
            }else{
                $objek ='';
            }
        }else{
            $objek ='';
        }
        return $objek;
        // $data = \App\Models\Act\ActPengujian::whereIn('parent_id', $detail)->orderBy('created_at','desc')->get();
    }
}

if (!function_exists('getstandarisasi')) {
    function getstandarisasi($id)
    {
        $standarisasi = \App\Models\Master\StandarisasiTemuan::find($id);
        return $standarisasi->kode.'-'.$standarisasi->deskripsi;
    }
}

if (!function_exists('getstandarisasis')) {
    function getstandarisasis($id)
    {
        $standarisasi = \App\Models\Master\StandarisasiTemuan::find($id);
        return $standarisasi->deskripsi;
    }
}

if (!function_exists('getrapat')) {
    function getrapat()
    {
    	$rapat = \App\Models\Rapat\RapatInternal::where('tanggal', '>=', date("d-m-Y"))->first();
    	if($rapat){
    		if($rapat->tanggal == date("d-m-Y")){
	    		if(date("H:i") <= $rapat->jam_selesai){
	    			$rapat = $rapat;
	    		}else{
	    			$rapat = null;
	    		}
    		}else{
    			$rapat = $rapat;
    		}
    	}else{
    		$rapat =null;
    	}
    	return $rapat;
    }
}

if (!function_exists('getfileskka')) {
    function getfileskka($id)
    {
    	$cari = App\Models\Files::whereIn('target_id', $id)->where('target_type', 'kka')->get();
    	return $cari->pluck('url','target_id')->toArray();
    }
}

if (!function_exists('getkriteria')) {
    function getkriteria($id)
    {
        $kriteria = \App\Models\Master\StandarisasiTemuanDetail::where('standarisasi_id', $id)->get();
        return $kriteria;
    }
}

if (!function_exists('getkondisi')) {
    function getkondisi($id)
    {
        $kuy = $id;
        $cek = \App\Models\Master\StandarisasiTemuan::find($kuy);
        $cik='';
        foreach($cek->detail as $data){
            $cik.= '<li class="item cek-cek" style="text-align:justify">'.readMoreText($data->deskripsi, 100).'</li>';
        }
        return $cik;
    }
}

if (!function_exists('getkategoristatus')) {
    function getkategoristatus($id)
    {
        $kuy = $id;
        $cek = \App\Models\Master\KategoriTemuan::find($kuy);
        return $cek->nama;
    }
}

if (!function_exists('getstatus')) {
    function getstatus($id, $idx)
    {
        $cari = App\Models\Master\KertasKerjaDetail::where('kertas_kerja_id', $id)->where('pembobotan_detail_id', $idx)->first();
        return $cari->status;
    }
}

if (!function_exists('getstatusa')) {
    function getstatusa($id, $idx, $idy)
    {
        $cari = App\Models\Master\KertasKerjaDetail::where('kertas_kerja_id', $id)->where('pembobotan_detail_id', $idx)->first();
        $cari2 = App\Models\Master\KertasKerjaDetailNilai::where('detail_id', $cari->id)->where('detail_nilai_id', $idy)->first();
        return $cari2->status;
    }
}

if (!function_exists('getmaxparent')) {
    function getmaxparent($id)
    {
        $cari2 = App\Models\KegiatanAudit\Rencana\RencanaAudit::where('parent_id', $id)->get();
        return $cari2->max('revisi');
    }
}

//Jieunan Stef GG
if (!function_exists('checkunique')) {
    function checkunique($data, $shouldUnique=[])
    {
        $exist = collect([]);
        foreach ($data as $key => $value) {
            $notunique = $exist->filter(function ($item) use ($value, $shouldUnique) {
                $count = 0;
                foreach ($shouldUnique as $unique) {
                    $count += $value[$unique] == $item[$unique] ? 1 : 0;
                }
                return $count >= count($shouldUnique);
            });

            if($notunique->count() > 0){
                return false;
            }

            $exist->push($value);
        }

        return true;
    }
}

if (!function_exists('filter_array')) {
    function filter_array($array,$term)
    {
        $matches = array();
        foreach($array as $a){
            if($a['tipe'] == $term)
                $matches[]=$a;
        }
        return $matches;
    }
}

if (!function_exists('getNotif')) {
    function getNotif($user)
    {
        $data = \App\Models\Notification::where('user_id', $user->id)->orderBy('created_at','desc')->get();
        return $data;
    }
}

if (!function_exists('getNotifs')) {
    function getNotifs($user)
    {
        $data = \App\Models\Notification::where('user_id', $user->id)->where('status', 1)->get()->count();
        return $data;
    }
}


if (!function_exists('bubblehead')) {
    function bubblehead($menu, $user)
    {
        $head =0;
        if($menu == 'kegiatanAudit'){
            $penugasana=0;$penugasanb=0;$permintaana=0;$permintaanb=0;$programaudita=0; $programauditb=0;$openinga=0;$openingb=0;$closinga=0;$closingb=0;
            $kka=0; $kkb=0;$lha=0;$lhb=0;
            if($user->hasRole(['svp-audit','dirut','sekre-ia','auditor'])){
                $penugasana = \App\Models\KegiatanAudit\Rencana\RencanaAuditDetail::whereHas('rencanaaudit', function($u){
                                                $u->whereNotNull('bukti');
                                           })
                                           ->where('status_penugasan', 0)
                                           ->where('flag', 1)
                                           ->where('tipe', 0)
                                           ->get()->count();
                $penugasanb = \App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit::where('status', '!=', 0)
                                           ->where('status', '!=', 4)
                                           ->get()->count();
            }
            if($user->hasRole(['auditor','dirut','svp-audit'])){
                $permintaana = \App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit::where('status_tinjauan', 0)->whereIn('status', [1,2,3,4])->get()->count();
                $permintaanb = \App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen::whereIn('status', [1,2,3,4])->get()->count();
            }elseif($user->hasRole(['svp','kapro'])){
                $permintaana = 0;
                $cari = checkuser($user->id);
				$tipe_object = $cari[0];
				$object_id = $cari[1];
                $permintaanb = \App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen::whereHas('penugasanaudit', function($x) use ($tipe_object, $object_id){
														    										$x->whereHas('rencanadetail', function($y) use ($tipe_object, $object_id){
														    											$y->where('tipe_object', $tipe_object)
														    											  ->whereIn('object_id', $object_id);
														    										});
														    									})
														    									->whereIn('status', [2,3,4])
														    									->get()->count();
            }else{
                $permintaana = 0;
                $permintaanb = 0;
            }
            if($user->hasRole(['dirut','svp-audit','sekre-ia'])){
                $programaudita = \App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit::where('status_audit', 0)->whereIn('status', [1,2,3,4])->get()->count();
                $programauditb = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereIn('status', [1,2,3])->get()->count();
            }elseif($user->hasRole(['auditor'])){
            	$programaudita = \App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit::whereHas('anggota', function($u) use ($user){
														       	 								 $u->where('fungsi', 0)->where('user_id', $user->id);
														       	 							   })
														       	 							   ->where('status_audit', 0)->whereIn('status', [1,2,3,4])->get()->count();
                $programauditb = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereHas('detailanggota', function($u) use ($user){
															    									$u->where('user_id', $user->id);
															    								})
                																				->orWhere('user_id', $user->id)
															    								->whereIn('status', [1,2,3])->get()->count();
            }else{
            	$programaudita = 0;
                $programauditb = 0;
            }
            $angka_penugasan = $penugasana + $penugasanb;
            $angka_permintaan = $permintaana + $permintaanb;
            $angka_programaudit = $programaudita + $programauditb;

            $child_persiapan = $angka_penugasan + $angka_permintaan + $angka_programaudit;

            if($user->hasRole(['auditor','dirut','svp-audit'])){
                $openinga = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereHas('penugasanaudit', function($u){
															        							$u->whereHas('tinjauandokumen', function($a){
															        								$a->where('status', 5);
															        							});
															        						})->doesntHave('opening')->whereIn('status', [4,5])->get()->count();
                $openingb = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::where('status', 1)->get()->count();
            }elseif($user->hasRole(['svp','kapro'])){
            	$openinga = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereHas('penugasanaudit', function($u){
															        							$u->whereHas('tinjauandokumen', function($a){
															        								$a->where('status', 5);
															        							});
															        						})->whereHas('detailanggota', function($u) use ($user){
													    										$u->where('user_id', $user->id);
													    								  	})->doesntHave('opening')->whereIn('status', [4,5])->get()->count();
                $openingb = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::whereHas('detailanggota', function($u) use ($user){
															        							$u->where('user_id', $user->id);
															        						})
															        						->orWhereHas('program', function($u) use ($user){
															        							$u->whereHas('detailanggota', function($a) use ($user){
															        								$a->where('user_id', $user->id);
															        							})->where('user_id', $user->id);
															        						})
															        						->where('status', 1)->get()->count();
            }else{
            	$openinga = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::where('id', 0)->get()->count();
       			$openingb = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::where('id', 0)->get()->count();
            }

            if($user->hasRole(['auditor','dirut','svp-audit'])){
                $closinga = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::doesntHave('closing')->where('status', 2)->get()->count();
                $closingb = \App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting::where('status', 1)->get()->count();
            }elseif($user->hasRole(['svp','kapro'])){
            	$closinga = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::whereHas('detailanggota', function($u) use ($user){
																        							$u->where('user_id', $user->id);
																        						 })
																        						 ->orWhereHas('program', function($u) use ($user){
																        							$u->whereHas('detailanggota', function($a) use ($user){
																        								$a->where('user_id', $user->id);
																        							})->where('user_id', $user->id);
																        						 })
																    						 	 ->doesntHave('closing')
																        						 ->where('status', 2)->get()->count();
                $closingb = \App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting::whereHas('opening', function($a) use ($user){
                																					$a->whereHas('detailanggota', function($u) use ($user){
																	        							$u->where('user_id', $user->id);
																	        						 })
																	        						 ->orWhereHas('program', function($u) use ($user){
																	        							$u->whereHas('detailanggota', function($a) use ($user){
																	        								$a->where('user_id', $user->id);
																	        							})->where('user_id', $user->id);
																	        						 });
																				                })
                																				->where('status', 1)->get()->count();
            }else{
            	$closinga = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::where('id', 0)->get()->count();
                $closingb = \App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting::where('id', 0)->get()->count();
            }
            
            if($user->hasRole(['svp-audit','dirut','sekre-ia'])){
	        	$kka = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereHas('penugasanaudit', function($u){
														        							$u->whereHas('tinjauandokumen', function($a){
														        								$a->where('status', 5);
														        							});
														        						})->where('status_kka', 0)->whereIn('status', [4,5])->get()->count();
	        	$kkb = \App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka::whereIn('status', [1,2,3,4])->get()->count();
	       	}elseif($user->hasRole(['auditor'])){
	        	$kka = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereHas('penugasanaudit', function($u){
													        							$u->whereHas('tinjauandokumen', function($a){
													        								$a->where('status', 5);
													        							});
													        						})->whereHas('detailanggota', function($u) use ($user){
												    									$u->where('user_id', $user->id);
												    								})
													        						->where('status_kka', 0)->whereIn('status', [4,5])->get()->count();
	        	$kkb = \App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka::whereHas('program', function($a) use ($user){
		        						$a->whereHas('detailanggota', function($u) use ($user){
	    									$u->where('user_id', $user->id);
	    								});
		        					})
		        					->whereIn('status', [1,2,3,4])->get()->count();
	       	}elseif($user->hasRole(['svp','kapro'])){
	        	$kka = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::where('id', 0)->get()->count();
	        	$kkb = \App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka::whereHas('detaildraft', function($a) use ($user){
	    								$a->where('user_id', $user->id);
		        					})
		        					->whereIn('status', [1,2,3,4])->get()->count();
	       	}else{
	       		$kka = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::where('id', 0)->get()->count();
	       		$kkb = \App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka::where('id', 0)->get()->count();
	       	}

            $angka_opening = $openinga + $openingb;
            $angka_closing = $closinga + $closingb;
            $angka_kka = $kka + $kkb;

            $child_pelaksanaan = $angka_opening + $angka_closing + $angka_kka;

            if($user->hasRole(['auditor','svp-audit','sekre-ia','dirut'])){
                $lha = \App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka::where('status', 5)->get()->count();
                $lhb = \App\Models\KegiatanAudit\Pelaporan\LHA::whereIn('status', [1,2,3,4,5])->get()->count();
            }

            $child_laporan = $lha + $lhb;

            $laporan = 0;$surat =0;$register = 0;
            if($user->hasRole(['dirut'])){
                $surat= 0;
            }
            if($user->hasRole(['auditor','dirut','svp-audit','svp','kapro','sekre-ia'])){
                $laporan = 0;
            }
            $register = 0;

            $child_tl = $surat + $laporan + $register;

            $survey = \App\Models\Survey\SurveyJawab::badgeTab('survey')->total;
            $hasil = \App\Models\Survey\SurveyJawab::badgeTab('hasil')->total;

            $head =$child_persiapan+$child_pelaksanaan+$child_laporan+$child_tl+$survey+$hasil;

        }elseif($menu == 'rKIA'){
        	$angka=0;$angkb=0;
        	if($user->hasRole(['svp-audit', 'dirut','auditor','sekre-ia'])){
                $angka = \App\Models\KegiatanAudit\Rencana\RencanaAudit::where('tipe', 0)
                                                            ->whereIn('status', [0,1,2,3])->get()->count();
                $angkb = \App\Models\KegiatanAudit\Rencana\RencanaAudit::where('tipe', 1)
                                                            ->whereIn('status', [0,1,2,3])->get()->count();
            }
            $head = $angka + $angkb;
        }elseif($menu == 'kegiatanKonsultasi'){
            if($user->hasRole(['svp-audit', 'dirut','auditor','sekre-ia']) || $user->isUserIa()){
                $perencanaan = \App\Models\KegiatanKonsultasi\KegiatanKonsultasi::badgeTab('perencanaan')->total;
                $pelaksanaan = \App\Models\KegiatanKonsultasi\KegiatanKonsultasi::badgeTab('pelaksanaan')->total;
                $laporan     = \App\Models\KegiatanKonsultasi\KegiatanKonsultasi::badgeTab('laporan')->total;
                $head = $perencanaan + $pelaksanaan + $laporan;
            }
        }elseif($menu == 'kegiatanLainnya'){
            if($user->hasRole(['svp-audit', 'dirut','auditor','sekre-ia']) || $user->isUserIa()){
                $perencanaan = \App\Models\KegiatanLainnya\KegiatanLainnya::badgeTab('perencanaan')->total;
                $pelaksanaan = \App\Models\KegiatanLainnya\KegiatanLainnya::badgeTab('pelaksanaan')->total;
                $laporan     = \App\Models\KegiatanLainnya\KegiatanLainnya::badgeTab('laporan')->total;
                $head = $perencanaan + $pelaksanaan + $laporan;
            }
        }elseif($menu == 'evaluasiMutuAudit'){
        }elseif($menu == 'rapat'){
            $internal = \App\Models\Rapat\RapatInternal::where('status', 0)->get()->count();
            $advisor = \App\Models\Rapat\RapatInternal::where('status', 1)->get()->count();
            $konsultasi = \App\Models\Rapat\RapatInternal::where('status', 3)->get()->count();
            $eksternal = \App\Models\Rapat\RapatInternal::where('status', 2)->get()->count();
            $head = $internal+$advisor+$konsultasi+$eksternal;
        }else{
            $head =0;
        }

        return $head;
    }
}

if (!function_exists('bubblechild')) {
    function bubblechild($menu, $user)
    {
        $child =0;$penugasana=0;$penugasanb=0;$permintaana=0;$permintaanb=0;$programaudita=0; $programauditb=0;$openinga=0;$openingb=0;$closinga=0;$closingb=0;$kka=0; $kkb=0;$lha=0;$lhb=0;
        if($menu == 'persiapan'){
            $penugasana=0;$penugasanb=0;$permintaana=0;$permintaanb=0;$programaudita=0;$programauditb=0;
            if($user->hasRole(['svp-audit','dirut','sekre-ia','auditor'])){
                $penugasana = \App\Models\KegiatanAudit\Rencana\RencanaAuditDetail::whereHas('rencanaaudit', function($u){
                                                $u->whereNotNull('bukti');
                                           })
                                           ->where('status_penugasan', 0)
                                           ->where('flag', 1)
                                           ->where('tipe', 0)
                                           ->get()->count();
                $penugasanb = \App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit::where('status', '!=', 0)
                                           ->where('status', '!=', 4)
                                           ->get()->count();
            }
            if($user->hasRole(['auditor','dirut','svp-audit'])){
                $permintaana = \App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit::where('status_tinjauan', 0)->whereIn('status', [1,2,3,4])->get()->count();
                $permintaanb = \App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen::whereIn('status', [1,2,3])->get()->count();
            }elseif($user->hasRole(['svp','kapro'])){
                $permintaana = 0;
                $cari = checkuser($user->id);
				$tipe_object = $cari[0];
				$object_id = $cari[1];
                $permintaanb = \App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen::whereHas('penugasanaudit', function($x) use ($tipe_object, $object_id){
														    										$x->whereHas('rencanadetail', function($y) use ($tipe_object, $object_id){
														    											$y->where('tipe_object', $tipe_object)
														    											  ->whereIn('object_id', $object_id);
														    										});
														    									})
														    									->whereIn('status', [2,3,4])->get()->count();
            }else{
                $permintaana = 0;
                $permintaanb = 0;
            }

            if($user->hasRole(['dirut','svp-audit','sekre-ia'])){
                $programaudita = \App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit::where('status_audit', 0)->whereIn('status', [1,2,3,4])->get()->count();
                $programauditb = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereIn('status', [1,2,3])->get()->count();
            }elseif($user->hasRole(['auditor'])){
            	$programaudita = \App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit::whereHas('anggota', function($u) use ($user){
														       	 								 $u->where('fungsi', 0)->where('user_id', $user->id);
														       	 							   })
														       	 							   ->where('status_audit', 0)->whereIn('status', [1,2,3,4])->get()->count();
                $programauditb = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereHas('detailanggota', function($u) use ($user){
															    									$u->where('user_id', $user->id);
															    								})
                																				->orWhere('user_id', $user->id)
															    								->whereIn('status', [1,2,3])->get()->count();
            }else{
            	$programaudita = 0;
                $programauditb = 0;
            }
            $angka_penugasan = $penugasana + $penugasanb;
            $angka_permintaan = $permintaana + $permintaanb;
            $angka_programaudit = $programaudita + $programauditb;
            $child = $angka_penugasan + $angka_permintaan + $angka_programaudit;
        }elseif($menu == 'pelaksanaan'){
            $openinga=0;$openingb=0;$closinga=0;$closingb=0;$kka=0;$kkb=0;

            if($user->hasRole(['auditor','dirut','svp-audit'])){
                $openinga = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereHas('penugasanaudit', function($u){
															        							$u->whereHas('tinjauandokumen', function($a){
															        								$a->where('status', 5);
															        							});
															        						})->doesntHave('opening')->whereIn('status', [4,5])->get()->count();
                $openingb = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::where('status', 1)->get()->count();
            }elseif($user->hasRole(['svp','kapro'])){
            	$openinga = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereHas('penugasanaudit', function($u){
															        							$u->whereHas('tinjauandokumen', function($a){
															        								$a->where('status', 5);
															        							});
															        						})->whereHas('detailanggota', function($u) use ($user){
													    										$u->where('user_id', $user->id);
													    								  	})->doesntHave('opening')->whereIn('status', [4,5])->get()->count();
                $openingb = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::whereHas('detailanggota', function($u) use ($user){
															        							$u->where('user_id', $user->id);
															        						})
															        						->orWhereHas('program', function($u) use ($user){
															        							$u->whereHas('detailanggota', function($a) use ($user){
															        								$a->where('user_id', $user->id);
															        							})->where('user_id', $user->id);
															        						})
															        						->where('status', 1)->get()->count();
            }else{
            	$openinga = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::where('id', 0)->get()->count();
       			$openingb = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::where('id', 0)->get()->count();
            }

            
            if($user->hasRole(['auditor','dirut','svp-audit'])){
                $closinga = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::doesntHave('closing')->where('status', 2)->get()->count();
                $closingb = \App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting::where('status', 1)->get()->count();
            }elseif($user->hasRole(['svp','kapro'])){
            	$closinga = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::whereHas('detailanggota', function($u) use ($user){
																        							$u->where('user_id', $user->id);
																        						 })
																        						 ->orWhereHas('program', function($u) use ($user){
																        							$u->whereHas('detailanggota', function($a) use ($user){
																        								$a->where('user_id', $user->id);
																        							})->where('user_id', $user->id);
																        						 })
																    						 	 ->doesntHave('closing')
																        						 ->where('status', 2)->get()->count();
                $closingb = \App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting::whereHas('opening', function($a) use ($user){
                																					$a->whereHas('detailanggota', function($u) use ($user){
																	        							$u->where('user_id', $user->id);
																	        						 })
																	        						 ->orWhereHas('program', function($u) use ($user){
																	        							$u->whereHas('detailanggota', function($a) use ($user){
																	        								$a->where('user_id', $user->id);
																	        							})->where('user_id', $user->id);
																	        						 });
																				                })
                																				->where('status', 1)->get()->count();
            }else{
            	$closinga = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::where('id', 0)->get()->count();
                $closingb = \App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting::where('id', 0)->get()->count();
            }

            
            if($user->hasRole(['svp-audit','dirut','sekre-ia'])){
	        	$kka = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereHas('penugasanaudit', function($u){
													        							$u->whereHas('tinjauandokumen', function($a){
													        								$a->where('status', 5);
													        							});
													        						})->where('status_kka', 0)->whereIn('status', [4,5])->get()->count();
	        	$kkb = \App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka::whereIn('status', [1,2,3,4])->get()->count();
	       	}elseif($user->hasRole(['auditor'])){
	        	$kka = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereHas('penugasanaudit', function($u){
													        							$u->whereHas('tinjauandokumen', function($a){
													        								$a->where('status', 5);
													        							});
													        						})->whereHas('detailanggota', function($u) use ($user){
												    									$u->where('user_id', $user->id);
												    								})
													        						->where('status_kka', 0)->whereIn('status', [4,5])->get()->count();
	        	$kkb = \App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka::whereHas('program', function($a) use ($user){
		        						$a->whereHas('detailanggota', function($u) use ($user){
	    									$u->where('user_id', $user->id);
	    								});
		        					})
		        					->whereIn('status', [1,2,3,4])->get()->count();
	       	}elseif($user->hasRole(['svp','kapro'])){
	        	$kka = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::where('id', 0)->get()->count();
	        	$kkb = \App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka::whereHas('detaildraft', function($a) use ($user){
	    								$a->where('user_id', $user->id);
		        					})
		        					->whereIn('status', [1,2,3,4])->get()->count();
	       	}else{
	       		$kka = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::where('id', 0)->get()->count();
	       		$kkb = \App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka::where('id', 0)->get()->count();
	       	}

            $angka_opening = $openinga + $openingb;
            $angka_closing = $closinga + $closingb;
            $angka_kka = $kka + $kkb;

            $child = $angka_opening + $angka_closing + $angka_kka;
        }elseif($menu == 'pelaporan'){
            $lha=0;$lhb=0;
            if($user->hasRole(['auditor','svp-audit','sekre-ia','dirut'])){
                $lha = \App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka::where('status', 5)->get()->count();
                $lhb = \App\Models\KegiatanAudit\Pelaporan\LHA::whereIn('status', [1,2,3,4,5])->get()->count();
            }
            $child = $lha + $lhb;
        }elseif($menu == 'tindakLanjut'){
            $laporan = 0;$surat =0;$register = 0;
            if($user->hasRole(['dirut'])){
                $surat= 0;
            }
            if($user->hasRole(['auditor','dirut','svp-audit','svp','kapro','sekre-ia'])){
                $laporan = 0;
            }
            $register = 0;

            $child = $surat + $laporan + $register;
        }elseif($menu == 'surveyKepuasanAudit'){
            $survey = \App\Models\Survey\SurveyJawab::badgeTab('survey')->total;
            $hasil = \App\Models\Survey\SurveyJawab::badgeTab('hasil')->total;
            $child = $survey + $hasil;
        }elseif($menu == 'kegiatanKonsultasiPerencanaan'){
            if($user->hasRole(['auditor','svp-audit','dirut','sekre-ia']) || $user->isUserIa()){
                $child = \App\Models\KegiatanKonsultasi\KegiatanKonsultasi::badgeTab('perencanaan')->total;
            }
        }elseif($menu == 'kegiatanKonsultasiPelaksanaan'){
            if($user->hasRole(['auditor','svp-audit','dirut','sekre-ia']) || $user->isUserIa()){
                $child = \App\Models\KegiatanKonsultasi\KegiatanKonsultasi::badgeTab('pelaksanaan')->total;
            }
        }elseif($menu == 'kegiatanKonsultasiLaporan'){
            if($user->hasRole(['auditor','svp-audit','dirut','sekre-ia']) || $user->isUserIa()){
                $child = \App\Models\KegiatanKonsultasi\KegiatanKonsultasi::badgeTab('laporan')->total;
            }
        }elseif($menu == 'kegiatanLainnyaPerencanaan'){
            if($user->hasRole(['auditor','svp-audit','dirut','sekre-ia']) || $user->isUserIa()){
                $child = \App\Models\KegiatanLainnya\KegiatanLainnya::badgeTab('perencanaan')->total;
            }
        }elseif($menu == 'kegiatanLainnyaPelaksanaan'){
            if($user->hasRole(['auditor','svp-audit','dirut','sekre-ia']) || $user->isUserIa()){
                $child = \App\Models\KegiatanLainnya\KegiatanLainnya::badgeTab('pelaksanaan')->total;
            }
        }elseif($menu == 'kegiatanLainnyaLaporan'){
            if($user->hasRole(['auditor','svp-audit','dirut','sekre-ia']) || $user->isUserIa()){
                $child = \App\Models\KegiatanLainnya\KegiatanLainnya::badgeTab('laporan')->total;
            }
        }elseif($menu == 'internal'){
            $internal = \App\Models\Rapat\RapatInternal::where('status', 0)->get()->count();
            $child = $internal;
        }elseif($menu == 'eksternal'){
            $advisor = \App\Models\Rapat\RapatInternal::where('status', 1)->get()->count();
            $konsultasi = \App\Models\Rapat\RapatInternal::where('status', 3)->get()->count();
            $eksternal = \App\Models\Rapat\RapatInternal::where('status', 2)->get()->count();
            $child =$advisor+$konsultasi+$eksternal;
        }else{
            $child =0;
        }
        return $child;
    }
}

if (!function_exists('bubblechilds')) {
    function bubblechilds($menu, $user)
    {   
        $angka =0;
        if($menu == 'suratPenugasan'){
            $penugasana=0;$penugasanb=0;
            if($user->hasRole(['svp-audit','dirut','sekre-ia','auditor'])){
                $penugasana = \App\Models\KegiatanAudit\Rencana\RencanaAuditDetail::whereHas('rencanaaudit', function($u){
                                                $u->whereNotNull('bukti');
                                           })
                                           ->where('status_penugasan', 0)
                                           ->where('flag', 1)
                                           ->where('tipe', 0)
                                           ->get()->count();
                $penugasanb = \App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit::where('status', '!=', 0)
                                           ->where('status', '!=', 4)
                                           ->get()->count();
            }
            $angka = $penugasana + $penugasanb;
        }elseif($menu == 'permintaanDokumen'){
            $permintaana=0;$permintaanb=0;
            if($user->hasRole(['auditor','dirut','svp-audit'])){
                $permintaana = \App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit::where('status_tinjauan', 0)->whereIn('status', [1,2,3,4])->get()->count();
                $permintaanb = \App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen::whereIn('status', [1,2,3,4])->get()->count();
                $angka = $permintaana + $permintaanb;
            }elseif($user->hasRole(['svp','kapro'])){
            	$cari = checkuser($user->id);
				$tipe_object = $cari[0];
				$object_id = $cari[1];
                $permintaanb = \App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen::whereHas('penugasanaudit', function($x) use ($tipe_object, $object_id){
														    										$x->whereHas('rencanadetail', function($y) use ($tipe_object, $object_id){
														    											$y->where('tipe_object', $tipe_object)
														    											  ->whereIn('object_id', $object_id);
														    										});
														    									})
														    									->whereIn('status', [2,3,4])->get()->count();
                $angka = $permintaanb;
            }else{
                $angka = 0;
            }
        }elseif($menu == 'programAudit'){
            $programaudita=0;$programauditb=0;
            if($user->hasRole(['dirut','svp-audit','sekre-ia'])){
                $programaudita = \App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit::where('status_audit', 0)->whereIn('status', [1,2,3,4])->get()->count();
                $programauditb = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereIn('status', [1,2,3])->get()->count();
            }elseif($user->hasRole(['auditor'])){
            	$programaudita = \App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit::whereHas('anggota', function($u) use ($user){
														       	 								 $u->where('fungsi', 0)->where('user_id', $user->id);
														       	 							   })
														       	 							   ->where('status_audit', 0)->whereIn('status', [1,2,3,4])->get()->count();
                $programauditb = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereHas('detailanggota', function($u) use ($user){
															    									$u->where('user_id', $user->id);
															    								})
                																				->orWhere('user_id', $user->id)
															    								->whereIn('status', [1,2,3])->get()->count();
            }else{
            	$programaudita = 0;
                $programauditb = 0;
            }
            $angka = $programaudita + $programauditb;
        }elseif($menu == 'openingMeeting'){
            $openinga=0;$openingb=0;
            if($user->hasRole(['auditor','dirut','svp-audit'])){
                $openinga = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereHas('penugasanaudit', function($u){
															        							$u->whereHas('tinjauandokumen', function($a){
															        								$a->where('status', 5);
															        							});
															        						})->doesntHave('opening')->whereIn('status', [4,5])->get()->count();
                $openingb = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::where('status', 1)->get()->count();
            }elseif($user->hasRole(['svp','kapro'])){
            	$openinga = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereHas('penugasanaudit', function($u){
															        							$u->whereHas('tinjauandokumen', function($a){
															        								$a->where('status', 5);
															        							});
															        						})->whereHas('detailanggota', function($u) use ($user){
													    										$u->where('user_id', $user->id);
													    								  	})->doesntHave('opening')->whereIn('status', [4,5])->get()->count();
                $openingb = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::whereHas('detailanggota', function($u) use ($user){
															        							$u->where('user_id', $user->id);
															        						})
															        						->orWhereHas('program', function($u) use ($user){
															        							$u->whereHas('detailanggota', function($a) use ($user){
															        								$a->where('user_id', $user->id);
															        							})->where('user_id', $user->id);
															        						})
															        						->where('status', 1)->get()->count();
            }else{
            	$openinga = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::where('id', 0)->get()->count();
       			$openingb = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::where('id', 0)->get()->count();
            }
            $angka = $openinga + $openingb;
        }elseif($menu == 'closingMeeting'){
            $closinga=0;$closingb=0;
            if($user->hasRole(['auditor','dirut','svp-audit'])){
                $closinga = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::doesntHave('closing')->where('status', 2)->get()->count();
                $closingb = \App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting::where('status', 1)->get()->count();
            }elseif($user->hasRole(['svp','kapro'])){
            	$closinga = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::whereHas('detailanggota', function($u) use ($user){
																        							$u->where('user_id', $user->id);
																        						 })
																        						 ->orWhereHas('program', function($u) use ($user){
																        							$u->whereHas('detailanggota', function($a) use ($user){
																        								$a->where('user_id', $user->id);
																        							})->where('user_id', $user->id);
																        						 })
																    						 	 ->doesntHave('closing')
																        						 ->where('status', 2)->get()->count();
                $closingb = \App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting::whereHas('opening', function($a) use ($user){
                																					$a->whereHas('detailanggota', function($u) use ($user){
																	        							$u->where('user_id', $user->id);
																	        						 })
																	        						 ->orWhereHas('program', function($u) use ($user){
																	        							$u->whereHas('detailanggota', function($a) use ($user){
																	        								$a->where('user_id', $user->id);
																	        							})->where('user_id', $user->id);
																	        						 });
																				                })
                																				->where('status', 1)->get()->count();
            }else{
            	$closinga = \App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting::where('id', 0)->get()->count();
                $closingb = \App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting::where('id', 0)->get()->count();
            }
            $angka = $closinga + $closingb;
        }elseif($menu == 'kKA'){
            $kka=0;$kkb=0;

            if($user->hasRole(['svp-audit','dirut','sekre-ia'])){
	        	$kka = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereHas('penugasanaudit', function($u){
													        							$u->whereHas('tinjauandokumen', function($a){
													        								$a->where('status', 5);
													        							});
													        						})->where('status_kka', 0)->whereIn('status', [4,5])->get()->count();
	        	$kkb = \App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka::whereIn('status', [1,2,3,4])->get()->count();
	       	}elseif($user->hasRole(['auditor'])){
	        	$kka = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::whereHas('penugasanaudit', function($u){
													        							$u->whereHas('tinjauandokumen', function($a){
													        								$a->where('status', 5);
													        							});
													        						})->whereHas('detailanggota', function($u) use ($user){
												    									$u->where('user_id', $user->id);
												    								})
													        						->where('status_kka', 0)->whereIn('status', [4,5])->get()->count();
	        	$kkb = \App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka::whereHas('program', function($a) use ($user){
		        						$a->whereHas('detailanggota', function($u) use ($user){
	    									$u->where('user_id', $user->id);
	    								});
		        					})
		        					->whereIn('status', [1,2,3,4])->get()->count();
	       	}elseif($user->hasRole(['svp','kapro'])){
	        	$kka = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::where('id', 0)->get()->count();
	        	$kkb = \App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka::whereHas('detaildraft', function($a) use ($user){
	    								$a->where('user_id', $user->id);
		        					})
		        					->whereIn('status', [1,2,3,4])->get()->count();
	       	}else{
	       		$kka = \App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit::where('id', 0)->get()->count();
	       		$kkb = \App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka::where('id', 0)->get()->count();
	       	}
	       	
            $angka = $kka + $kkb;
        }elseif($menu == 'lHA'){
            $lha=0;$lhb=0;
            if($user->hasRole(['auditor','svp-audit','sekre-ia','dirut'])){
                $lha = \App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka::where('status', 5)->get()->count();
                $lhb = \App\Models\KegiatanAudit\Pelaporan\LHA::whereIn('status', [1,2,3,4,5])->get()->count();
            }
            $angka = $lha + $lhb;
        }elseif($menu == 'suratPerintahTL'){
            $surat =0;
            if($user->hasRole(['dirut'])){
                $surat= 0;
            }
            $angka = $surat;
        }elseif($menu == 'laporanTL'){
            $laporan = 0;
            if($user->hasRole(['auditor','dirut','svp-audit','svp','kapro','sekre-ia'])){
                $laporan = 0;
            }
            $angka = $laporan;
        }elseif($menu == 'monitoringTL'){
            $register = 0;
            $register = 0;
            $angka = $register;
        }elseif($menu == 'surveyKepuasanAuditSurvey'){
            $angka = \App\Models\Survey\SurveyJawab::badgeTab('survey')->total;
        }elseif($menu == 'surveyKepuasanAuditHasil'){
            $angka = \App\Models\Survey\SurveyJawab::badgeTab('hasil')->total;
        }elseif($menu == 'rapatAdvisor'){
            $advisor = \App\Models\Rapat\RapatInternal::where('status', 1)->get()->count();
            $angka = $advisor;
        }elseif($menu == 'rapatKonsultasi'){
            $konsultasi = \App\Models\Rapat\RapatInternal::where('status', 3)->get()->count();
            $angka = $konsultasi;
        }elseif($menu == 'rapatEksternal'){
            $eksternal = \App\Models\Rapat\RapatInternal::where('status', 2)->get()->count();
            $angka = $eksternal;
        }else{
            $angka = 0;
        }
        return $angka;
    }
}


